//
//  ActivitiesViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 02/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class ActivitiesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {

    @IBOutlet var activitiesTableView: UITableView!
    
    var allFitnessActivities: NSMutableArray! = NSMutableArray()
    var searchedActivities = [String]()
    var searchController = UISearchController()
    var fitnessItems: FitnessActivities = FitnessActivities()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "Select"
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.barTintColor = UIColor.whiteColor()
        self.activitiesTableView.tableHeaderView = self.searchController.searchBar
        allFitnessActivities = fitnessItems.populateFitnessActivities()
        activitiesTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (self.searchController.active)
        {
            return self.searchedActivities.count
        }
        else
        {
            return self.allFitnessActivities.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = activitiesTableView.dequeueReusableCellWithIdentifier("activityCell", forIndexPath: indexPath)
        
        if (self.searchController.active)
        {
            cell.textLabel!.text = self.searchedActivities[indexPath.row]
            return cell
        }
        else
        {
            cell.textLabel!.text = allFitnessActivities.objectAtIndex(indexPath.row) as? String
        }        
        //----------
        return cell
    }
    
    // Search bar is based on the Tutorial found on veasoftware.com/posts/uisearchcontroller-in-swift-xcode-7-ios-9-tutorial
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        self.searchedActivities.removeAll(keepCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (self.allFitnessActivities as NSArray).filteredArrayUsingPredicate(searchPredicate)
        self.searchedActivities = array as! [String]
        self.activitiesTableView.reloadData()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showActivityAddView", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showActivityAddView")
        {
            let upcoming: AddActivityViewController = segue.destinationViewController as! AddActivityViewController
            let indexPath = activitiesTableView.indexPathForSelectedRow!
            var titleString = ""
            if (self.searchController.active)
            {
                titleString = searchedActivities[indexPath.row]
            }
            else
            {
                titleString = allFitnessActivities.objectAtIndex(indexPath.row) as! String
            }
            upcoming.screenTitle = titleString
            activitiesTableView.deselectRowAtIndexPath(indexPath, animated: true)
            self.searchController.active = false

        }
        
    }


}
