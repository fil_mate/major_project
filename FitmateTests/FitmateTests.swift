//
//  FitmateTests.swift
//  FitmateTests
//
//  Created by Filip Kunkiewicz on 24/02/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import XCTest
@testable import Fitmate
import HealthKit

class FitmateTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    //---------------------------------------------------
    //ProfileMainViewController Tests
    //---------------------------------------------------
    func testProfileViewControllerSections()
    {
        let ProfileMainVC = ProfileMainViewController()
        let someTableView = UITableView()
        XCTAssert(ProfileMainVC.numberOfSectionsInTableView(someTableView) == 2)
    }
    
    func testProfileMainNumberOfRows()
    {
        let profileVC = ProfileMainViewController()
        let infoArray = profileVC.userInfo
        let tableView = UITableView()
        let numberOfRows = profileVC.tableView(tableView, numberOfRowsInSection: 1)
        XCTAssert(numberOfRows == infoArray.count)
    }
    
    func testProfileMainNumberOfSections()
    {
        let profileVC = ProfileMainViewController()
        let tableView = UITableView()
        let numberOfSections = profileVC.numberOfSectionsInTableView(tableView)
        XCTAssert(numberOfSections == 2)
    }
    
    func testProfileViewHeaderCell()
    {
        let profileVC = ProfileMainViewController()
        let tableView = UITableView()
        let HeaderHeight = profileVC.tableView(tableView, heightForHeaderInSection: 0)
        XCTAssert(HeaderHeight == 45)
    }
    
    func testProfileViewForHeader()
    {
        let profileVC = ProfileMainViewController()
        let tableView = UITableView()
        let viewForHeader = profileVC.tableView(tableView, viewForHeaderInSection: 0)
        let cellHeader = viewForHeader as! UITableViewCell
        let headerLabel = cellHeader.textLabel!.text
        XCTAssert(headerLabel == "User Information")
    }
    
    func testSeguePreparationProfileMainVC()
    {
        let profileVC = ProfileMainViewController()
        let segue = UIStoryboardSegue(identifier: "showUserInfoEditView", source: profileVC, destination: EditUserInfoViewController())
        let segueString = segue.identifier
        XCTAssert(segueString == "showUserInfoEditView")
    }
    //---------------------------------------------------
    //EditUserInfoViewController Tests
    //---------------------------------------------------
    func testUserInfoAgeString()
    {
        let EditUserInfoView = EditUserInfoViewController()
        XCTAssert(EditUserInfoView.getTextForTextField("Age") == "Your Age has to be set once, but it has to be done through the Apple Health App. \nIf you with to set or change it, please do so in Health -> Health Data -> Me.")
    }
    
    func testUserStatisticsBMIString()
    {
        let EditUserInfoView = EditUserInfoViewController()
        XCTAssert(EditUserInfoView.getTextForTextField("BMI") == "The Body Mass Index (BMI) is used as a measure, to see if you are healthy or not. Your BMI is calculated by Fitmate based on your Weight and Height. \n\nBMI < 18.5 = Underweight \nBMI 18.5–24.9 = Normal Weight \nBMI 25–29.9 = Overweight \nBMI > 30 = Obese\nThese estimate values are provided by the US National Heart, Lung, and Blood Insitute. They apply to both, men and women.")
    }
    
    //---------------------------------------------------
    //UserInformation Tests
    //---------------------------------------------------
    func testUserInfoClass()
    {
        let UserInfo = UserInformation()
        UserInfo.userAge = 20
        UserInfo.userHeight = 1.8
        UserInfo.userWeight = 84.3
        UserInfo.userSex = "Male"
        XCTAssert(UserInfo.userAge == 20 && UserInfo.userSex == "Male" && UserInfo.userHeight == 1.8 && UserInfo.userWeight == 84.3)
    }
    
    //---------------------------------------------------
    //ActivitiesViewController
    //---------------------------------------------------
    func testIfAllActivitiesLoadedIntoRows()
    {
        let activitiesVC = ActivitiesViewController()
        let fitActivities = FitnessActivities()
        let arrayOfActivities = fitActivities.populateFitnessActivities()
        activitiesVC.allFitnessActivities = arrayOfActivities
        let tableView = UITableView()
        let numberOfRows = activitiesVC.tableView(tableView, numberOfRowsInSection: 0)
        XCTAssert(numberOfRows == arrayOfActivities.count)
    }
    
    func testIfSearchedActivitiesLoaded()
    {
        let activitiesVC = ActivitiesViewController()
        activitiesVC.searchedActivities = ["First Activity", "Second Activity", "Third Activity"]
        XCTAssert(activitiesVC.searchedActivities.count == 3)
    }
    
    //---------------------------------------------------
    //FitnessActivities Tests
    //---------------------------------------------------
    func testPopulatingFitnessActivities()
    {
        let fitActivities = FitnessActivities()
        let fitnessActivities = fitActivities.populateFitnessActivities
        XCTAssert(fitnessActivities().count == 57)
    }
    
    func testIfCorrectActivityNameReturned()
    {
        let fitActivities = FitnessActivities()
        let activityNameString = fitActivities.getActivityName(HKWorkoutActivityType.FunctionalStrengthTraining)
        XCTAssert(activityNameString == "Functional Strength Training")
    }
    
    func testIfCorrectActivityTypeReturned()
    {
        let fitActivities = FitnessActivities()
        let activityType = fitActivities.getActivityType("American Football")
        XCTAssert(activityType == HKWorkoutActivityType.AmericanFootball)
    }
    
    //---------------------------------------------------
    // NutritionSamplesViewController Tests
    //---------------------------------------------------
    func testIfCorrectRowNumberForSamples()
    {
        let nutritionsSamplesVC = NutritionSamplesViewController()
        let date = NSDate()
        let quantity = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: 10.0)
        let quantityType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
        let nurtionObject1 = HKQuantitySample(type: quantityType!, quantity: quantity, startDate: date, endDate: date)
        let nurtionObject2 = HKQuantitySample(type: quantityType!, quantity: quantity, startDate: date, endDate: date)
        let nurtionObject3 = HKQuantitySample(type: quantityType!, quantity: quantity, startDate: date, endDate: date)
        nutritionsSamplesVC.nutritionSamples = [nurtionObject1, nurtionObject2, nurtionObject3]
        let numberOfRows = nutritionsSamplesVC.tableView(UITableView(), numberOfRowsInSection: 0)
        XCTAssert(numberOfRows == nutritionsSamplesVC.nutritionSamples.count)
    }
    

    //---------------------------------------------------
    // NutritionMenuItemsViewController Tests
    //---------------------------------------------------
    func testFoodItemNamesGeneration()
    {
        let nutritionMenuItems = NutritionMenuItemsViewController()
        let foodItemsArray = NSMutableArray()
        let foodItem1 = FoodItem()
        
        foodItem1.name = "Potato"
        foodItem1.calories = 345.2
        foodItemsArray.addObject(foodItem1)
        
        let foodItem2 = FoodItem()
        foodItem2.name = "Chicken"
        foodItem2.calories = 635.8
        foodItemsArray.addObject(foodItem2)

        let foodItem3 = FoodItem()
        foodItem3.name = "Rice"
        foodItem3.calories = 783.1
        foodItemsArray.addObject(foodItem3)

        let arrayOfNames = nutritionMenuItems.getNamesForFoodItems(foodItemsArray)
        XCTAssert(arrayOfNames == ["Potato", "Chicken", "Rice"])
    }
    
    func testGenerationOfFoodItemInformation()
    {
        let nutritionMenuItems = NutritionMenuItemsViewController()
        let foodItem = FoodItem()
        foodItem.name = "Chicken"
        foodItem.calories = 350.3
        foodItem.carbohydrates = 45.2
        foodItem.protein = 34.4
        foodItem.fats = 6.2
        let foodItemInfoString = nutritionMenuItems.generateFoodItemInfo(foodItem)
        let correctString = "Per 100g of Chicken: \nCalories: 350.3\nCarbohydrates: 45.2\nProtein: 34.4\nFats: 6.2"
        XCTAssert(foodItemInfoString == correctString)
    }
    
    //---------------------------------------------------
    // MenuItems Tests
    //---------------------------------------------------
    func testPopulationOfBreakfastAndLunchItems()
    {
        let menuItems = MenuItems()
        let currentBreakfastItemsNumber = 9
        let breakfastItemsArray = menuItems.populateBreakfastItems()
        XCTAssert(breakfastItemsArray.count == currentBreakfastItemsNumber)
    }
    
    func testPopulationOfDinnerItems()
    {
        let menuItems = MenuItems()
        let currentDinnerItemsNumber = 4
        let dinnerItemsArray = menuItems.populateDinnerItems()
        XCTAssert(dinnerItemsArray.count == currentDinnerItemsNumber)
    }
    func testPopulationOfSackItems()
    {
        let menuItems = MenuItems()
        let currentSnackItemsNumber = 6
        let snackItemsArray = menuItems.populateSnackItems()
        XCTAssert(snackItemsArray.count == currentSnackItemsNumber)
    }
    
    //---------------------------------------------------
    // Fitmate Universal
    //---------------------------------------------------
    func testGetDatesInBetweenInDiaryMain()
    {
        let universal = FitmateUniversal()
        let dateSevenDaysAgoString = "2016-04-03 15:34"
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let dateSevenDaysAgo = dateFormatter.dateFromString(dateSevenDaysAgoString)
        
        let todayDateString = "2016-04-10 21:21"
        let todayDate = dateFormatter.dateFromString(todayDateString)
        var nsDateArray = [NSDate]()
        let date1 = dateFormatter.dateFromString("2016-04-03 00:00")
        let date2 = dateFormatter.dateFromString("2016-04-04 00:00")
        let date3 = dateFormatter.dateFromString("2016-04-05 00:00")
        let date4 = dateFormatter.dateFromString("2016-04-06 00:00")
        let date5 = dateFormatter.dateFromString("2016-04-07 00:00")
        let date6 = dateFormatter.dateFromString("2016-04-08 00:00")
        let date7 = dateFormatter.dateFromString("2016-04-09 00:00")
        let date8 = dateFormatter.dateFromString("2016-04-10 00:00")
        nsDateArray.append(date1!)
        nsDateArray.append(date2!)
        nsDateArray.append(date3!)
        nsDateArray.append(date4!)
        nsDateArray.append(date5!)
        nsDateArray.append(date6!)
        nsDateArray.append(date7!)
        nsDateArray.append(date8!)

        XCTAssert(universal.getDatesInBetween(dateSevenDaysAgo!, endDate: todayDate!) == nsDateArray)
    }
    
    func testConvertDateWhenAddingNutrition()
    {
        let universal = FitmateUniversal()
        let dateString = "01-01-2015"
        let dateFromNutritionVC = universal.convertDate(dateString)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateString2 = "01-01-2015"
        let date2 = dateFormatter.dateFromString(dateString2)
        XCTAssert(dateFromNutritionVC == date2)
    }
    
    func testConvertTimeWhenAddingNutrition()
    {
        let universal = FitmateUniversal()
        let timeStringFromNutritionVC = "23:22"
        let timeFromNutritionVC = universal.convertTime(timeStringFromNutritionVC)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let timeString2 = "23:22"
        let time2 = dateFormatter.dateFromString(timeString2)
        XCTAssert(timeFromNutritionVC == time2)
    }
    
    
    func testCombineDateWithTimeWhenAddingActivitiess()
    {
        let universal = FitmateUniversal()
        let dateFormatterDate = NSDateFormatter()
        dateFormatterDate.dateFormat = "dd-MM-yyyy"
        let dateFormatterTime = NSDateFormatter()
        dateFormatterTime.dateFormat = "HH:mm"
        let dateString = "13-04-2016"
        let timeString = "17:44"
        let date = dateFormatterDate.dateFromString(dateString)
        let time = dateFormatterTime.dateFromString(timeString)
        let dateAndTimeFromActivityVC = universal.combineTimeWithDate(date!, time: time!)
        let dateAndTimeString = "13-04-2016 17:44"
        let dateFormatterAll = NSDateFormatter()
        dateFormatterAll.dateFormat = "dd-MM-yyyy HH:mm"
        let dateAndTime = dateFormatterAll.dateFromString(dateAndTimeString)
        XCTAssert(dateAndTimeFromActivityVC == dateAndTime)
    }
    
    //---------------------------------------------------
    // DailyStatisticsViewController
    //---------------------------------------------------
    func testForCorrectNumberOfRowsInSectionDiaryMain()
    {
        let diaryStatisticsVC = DailyStatisticsViewController()
        let tableView = UITableView()
        let numberOfRowsInSeciton2 = diaryStatisticsVC.tableView(tableView, numberOfRowsInSection: 1)
        XCTAssert(numberOfRowsInSeciton2 == 3)
    }
    
}
