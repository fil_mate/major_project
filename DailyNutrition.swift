//
//  DailyNutrition.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 01/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class DailyNutrition: NSObject {

    var date = NSDate()
    var totalCalories = 0.0
    var totalCarbohydrates = 0.0
    var totalProtein = 0.0
    var totalFats = 0.0
}
