//
//  EditUserInfoViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 04/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class EditUserInfoViewController: UIViewController {

    @IBOutlet var saveButton: UIBarButtonItem!
    @IBOutlet var textView: UITextView!
    @IBOutlet var valueField: UITextField!
    @IBOutlet var editInfoLabel: UILabel!
    var screenTitle = ""
    var healthManager: HealthKitManager?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        healthManager = HealthKitManager()
        valueField!.keyboardType = UIKeyboardType.DecimalPad
        self.title = screenTitle
        //-----------------------
        var labelString = ""
        labelString = "Enter " + screenTitle
        if(screenTitle == "Weight")
        {
           labelString += " in Kilograms"
        }
        if(screenTitle == "Height")
        {
            labelString += " in Meters"
        }
        editInfoLabel.text = labelString
        //-----------------------
        if(screenTitle != "Height" && screenTitle != "Weight")
        {
            valueField.hidden = true
            editInfoLabel.hidden = true
            saveButton.title = ""
            saveButton.enabled = false
            textView.text = getTextForTextField(screenTitle)
            textView.sizeToFit()
            textView.editable = false
        }
        else
        {
            textView.hidden = true
        }
    }

    //----------------------
    // Generate String based on which value was chosen
    //----------------------
    func getTextForTextField(userInfo: String) -> String
    {
        if(userInfo == "Age")
        {
            return "Your Age has to be set once, but it has to be done through the Apple Health App. \nIf you with to set or change it, please do so in Health -> Health Data -> Me."
        }
        else if(userInfo == "Sex")
        {
            return "Your Sex has to be set once, but it has to be done through the Apple Health App. \nIf you with to set or change it, please do so in Health -> Health Data -> Me."
        }
        else if(userInfo == "BMI")
        {
            // Values from www.nhlbi.nih.gov/health/educational/lose_wt/BMI/bmicalc.htm
            return "The Body Mass Index (BMI) is used as a measure, to see if you are healthy or not. Your BMI is calculated by Fitmate based on your Weight and Height. \n\nBMI < 18.5 = Underweight \nBMI 18.5–24.9 = Normal Weight \nBMI 25–29.9 = Overweight \nBMI > 30 = Obese\nThese estimate values are provided by the US National Heart, Lung, and Blood Insitute. They apply to both, men and women."
        }
        else if(userInfo == "BMR")
        {
            //info from dailyburn.com/life/health/how-to-calculate-bmr/
            //same link as calculation link
            return "The Basal Metabolic Rate (BMR) is the amount of energy (in the form of calories) that your body needs to function while resting for 24 hours. \n\nYour BMR is calculated by Fitmate using the Mifflin St. Jeor Equation, which takes into account your Age, Sex, Weight and Height."
        }
        else if(userInfo == "Body Fat")
        {
            //www.webmd.com/diet/body-fat-measurement?page=2
            return "Body Fat Percentage illustrates the percentage of your total weight, which is fat tissue. \nYour Body Fat Percentage is calculated by Fitmate, based on your Sex, Age and BMI. The following estimate values have been provided by WebMD.\n\nWomen: \nEssential fat: 10-12% \nAthletes: 14-20% \nFitness: 21-24% \nAcceptable: 25-31% \nObese: > 32% \n\nMen: \nEssential fat: 2-4% \nAthletes: 6-13% \nFitness: 14-17% \nAcceptable: 18-25% \nObese: > 26%"
        }
        else if(userInfo == "Lean Body Mass")
        {
            return "Lean Body Mass illustrates an amount of of your body weight, which isn't fat tissue. Fitmate calculates your Lean Body Mass based on your Body Fat Percentage and Body Weight."
        }
        return ""

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var valueToUpdate:Double?
    {
        return Double(valueField!.text!)
    }
    
    @IBAction func doSaveUserInfo(sender: AnyObject)
    {
        let dateNow = NSDate()
        if (valueField.hasText() == false )
        {
            let alertView = UIAlertController(title: "Error", message: "Please Enter " + screenTitle, preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        if(screenTitle == "Weight")
        {  
            healthManager?.saveUserWeight(valueToUpdate!, date: dateNow, completion: { (success, error ) -> Void in
                if( success )
                {
                    print("Weight saved")
                }
                else if( error != nil )
                {
                    print("Error saving weight" + "\(error)")
                }
            })
        }
        if(screenTitle == "Height")
        {
            healthManager?.saveUserHeight(valueToUpdate!, date: dateNow, completion: { (success, error ) -> Void in
                if( success )
                {
                    print("Height saved")
                }
                else if( error != nil )
                {
                    print("Error saving height" + "\(error)")
                }
            })
        }
    }
}
