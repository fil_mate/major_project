//
//  NutritionMenuItemsViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 22/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class NutritionMenuItemsViewController: UITableViewController, UISearchResultsUpdating {


    @IBOutlet var menuItemsTableView: UITableView!
    var screenTitle: String!
    var indexPathAfterAddSelection = 0
    var searchedNutrition = [String]()
    var searchController = UISearchController()
    
    var allItems: MenuItems = MenuItems()
    var menuItems: NSMutableArray! = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.barTintColor = UIColor.whiteColor()
        self.menuItemsTableView.tableHeaderView = self.searchController.searchBar
        menuItemsTableView.reloadData()
        //-------------------------------------------------
        self.title = screenTitle
        if (self.title == "Breakfast & Lunch")
        {
            menuItems = allItems.populateBreakfastItems()
        }
        else if (self.title == "Dinner")
        {
            menuItems = allItems.populateDinnerItems()
        }
        else
        {
            menuItems = allItems.populateSnackItems()
        }
        menuItems.sortUsingComparator{
            (foodItem1, foodItem2) -> NSComparisonResult in
            let item1 = foodItem1 as! FoodItem
            let item2 = foodItem2 as! FoodItem
            let result = item1.name.compare(item2.name)
            return result
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchController.active)
        {
            return searchedNutrition.count
        }
        else
        {
            return menuItems.count
        }
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        getSearchResults(menuItems)
        self.menuItemsTableView.reloadData()
    }
    
    func getSearchResults(menuArray: NSMutableArray)
    {
        self.searchedNutrition.removeAll(keepCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let foodItemNames = getNamesForFoodItems(menuArray)
        let array = foodItemNames.filteredArrayUsingPredicate(searchPredicate)
        self.searchedNutrition = array as! [String]
    }

    func getNamesForFoodItems(array: NSMutableArray) -> NSMutableArray
    {
        let foodItemNames: NSMutableArray! = NSMutableArray()
        for i in 0 ..< array.count
        {
            let FoodItemName = array.objectAtIndex(i).name!
            foodItemNames.addObject(FoodItemName)
        }
        return foodItemNames
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showNutritionItemView")
        {
            let upcoming: AddNutritionViewController = segue.destinationViewController as! AddNutritionViewController
            let indexPath = menuItemsTableView.indexPathForSelectedRow

            var titleString = ""
            if (searchController.active)
            {
                if (indexPath != nil)
                {
                    titleString = searchedNutrition[indexPath!.row]
                }
                else
                {
                    titleString = searchedNutrition[indexPathAfterAddSelection]
                }
            }
            else
            {
                if (indexPath != nil)
                {
                    titleString = menuItems[indexPath!.row].name
                }
                else
                {
                    titleString = menuItems[indexPathAfterAddSelection].name
                }
            }
            upcoming.foodName = titleString
            upcoming.menuItems = menuItems
            if(indexPath != nil)
            {
                menuItemsTableView.deselectRowAtIndexPath(indexPath!, animated: true)
            }
            self.searchController.active = false
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = menuItemsTableView.dequeueReusableCellWithIdentifier("nutritionItemCell", forIndexPath: indexPath)
        cell.accessoryType = .DetailDisclosureButton
        if ( searchController.active)
        {
            cell.textLabel!.text = searchedNutrition[indexPath.row]
        }
        else
        {
            cell.textLabel!.text = menuItems.objectAtIndex(indexPath.row).name
        }
        //----------
        return cell
    }
    
    override func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath)
    {
        var foodInfoString = ""
        var foodItem = FoodItem()
        if (self.searchController.active)
        {
            let foodItemName = searchedNutrition[indexPath.row]
            foodItem = getFoodItemFromName(foodItemName)
            foodInfoString = generateFoodItemInfo(foodItem)
        }
        else
        {
            foodItem = menuItems.objectAtIndex(indexPath.row) as! FoodItem
            foodInfoString = generateFoodItemInfo(foodItem)
        }
        
        let alertView = UIAlertController(title: "Nutritional Information", message:
        foodInfoString, preferredStyle: .Alert)
        alertView.addAction(UIAlertAction(title: "Add", style: .Default, handler: { (action: UIAlertAction!) in
            self.indexPathAfterAddSelection = indexPath.row
            self.performSegueWithIdentifier("showNutritionItemView", sender: self)
        }))
        
        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
        presentViewController(alertView, animated: true, completion: nil)
    }

    func getFoodItemFromName(foodItemName: String) -> FoodItem
    {
        var foodItem = FoodItem()
        for i in 0 ..< menuItems.count
        {
            if (foodItemName == menuItems.objectAtIndex(i).name!)
            {
                foodItem = menuItems.objectAtIndex(i) as! FoodItem
            }
        }
        return foodItem
    }
    
    func generateFoodItemInfo(foodItem: FoodItem) -> String
    {
        var messageString = ""
        messageString += "Per 100g of " + foodItem.name + ": \n"
        messageString += "Calories: " + String(foodItem.calories) + "\n"
        messageString += "Carbohydrates: " + String(foodItem.carbohydrates) + "\n"
        messageString += "Protein: " + String(foodItem.protein) + "\n"
        messageString += "Fats: " + String(foodItem.fats)
        return messageString
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showNutritionItemView", sender: self)
    }
    
    
}
