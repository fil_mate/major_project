//
//  MenuItems.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 23/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class MenuItems: NSObject {
    
    //--------------------------------------------------
    // BREAKFAST MENU
    //--------------------------------------------------
    func populateBreakfastItems() -> NSMutableArray
    {
        let breakfastAndLunchItems: NSMutableArray! = NSMutableArray()

        let cereal = FoodItem()
        cereal.name = "Cereal: Corn Flakes"
        cereal.calories = 360
        cereal.carbohydrates = 87
        cereal.protein = 7
        cereal.fats = 0
        breakfastAndLunchItems.addObject(cereal)
        
        let toastWhite = FoodItem()
        toastWhite.name = "Toast (White Bread)"
        toastWhite.calories = 293
        toastWhite.carbohydrates = 54
        toastWhite.protein = 9
        toastWhite.fats = 4
        breakfastAndLunchItems.addObject(toastWhite)
        
        let toastBrown = FoodItem()
        toastBrown.name = "Toast (Whole-Grain Bread)"
        toastBrown.calories = 288
        toastBrown.carbohydrates = 47
        toastBrown.protein = 15
        toastBrown.fats = 5
        breakfastAndLunchItems.addObject(toastBrown)

        let eggsBoiled = FoodItem()
        eggsBoiled.name = "Eggs: Boiled Or Raw"
        eggsBoiled.calories = 155
        eggsBoiled.carbohydrates = 1
        eggsBoiled.protein = 13
        eggsBoiled.fats = 11
        breakfastAndLunchItems.addObject(eggsBoiled)
        
        let eggsFried = FoodItem()
        eggsFried.name = "Eggs: Fried"
        eggsFried.calories = 196
        eggsFried.carbohydrates = 1
        eggsFried.protein = 14
        eggsFried.fats = 15
        breakfastAndLunchItems.addObject(eggsFried)
        
        let baconRaw = FoodItem()
        baconRaw.name = "Bacon: Raw"
        baconRaw.calories = 458
        baconRaw.carbohydrates = 1
        baconRaw.protein = 12
        baconRaw.fats = 45
        breakfastAndLunchItems.addObject(baconRaw)
        
        let baconFried = FoodItem()
        baconFried.name = "Bacon: Fried Or Roasted"
        baconFried.calories = 541
        baconFried.carbohydrates = 1
        baconFried.protein = 37
        baconFried.fats = 42
        breakfastAndLunchItems.addObject(baconFried)
        
        let peanutButter = FoodItem()
        peanutButter.name = "Peanut Butter"
        peanutButter.calories = 588
        peanutButter.carbohydrates = 20
        peanutButter.protein = 25
        peanutButter.fats = 50
        breakfastAndLunchItems.addObject(peanutButter)

        let cheeseCheddar = FoodItem()
        cheeseCheddar.name = "Cheese: Cheddar"
        cheeseCheddar.calories = 403
        cheeseCheddar.carbohydrates = 1
        cheeseCheddar.protein = 25
        cheeseCheddar.fats = 33
        breakfastAndLunchItems.addObject(cheeseCheddar)
        
        let cheeseGouda = FoodItem()
        cheeseGouda.name = "Cheese: Gouda"
        cheeseGouda.calories = 356
        cheeseGouda.carbohydrates = 2
        cheeseGouda.protein = 27
        cheeseGouda.fats = 25
        breakfastAndLunchItems.addObject(cheeseGouda)
        
        let cheeseMozzarella = FoodItem()
        cheeseMozzarella.name = "Cheese: Mozzarrela"
        cheeseMozzarella.calories = 300
        cheeseMozzarella.carbohydrates = 2
        cheeseMozzarella.protein = 22
        cheeseMozzarella.fats = 22
        breakfastAndLunchItems.addObject(cheeseMozzarella)
        
        let cheeseBrie = FoodItem()
        cheeseBrie.name = "Cheese: Brie"
        cheeseBrie.calories = 334
        cheeseBrie.carbohydrates = 0
        cheeseBrie.protein = 21
        cheeseBrie.fats = 28
        breakfastAndLunchItems.addObject(cheeseBrie)
        
        let salad = FoodItem()
        salad.name = "Salad"
        salad.calories = 600
        salad.carbohydrates = 2
        salad.protein = 7
        salad.fats = 6
        breakfastAndLunchItems.addObject(salad)
        
        let sausagePork = FoodItem()
        sausagePork.name = "Sausage: Breakfast (Pork)"
        sausagePork.calories = 339
        sausagePork.carbohydrates = 0
        sausagePork.protein = 19
        sausagePork.fats = 28
        breakfastAndLunchItems.addObject(sausagePork)
        
        let sausageFrankfurter = FoodItem()
        sausageFrankfurter.name = "Sausage: Hot Dog (Frankfurter)"
        sausageFrankfurter.calories = 290
        sausageFrankfurter.carbohydrates = 4.2
        sausageFrankfurter.protein = 10
        sausageFrankfurter.fats = 26
        breakfastAndLunchItems.addObject(sausageFrankfurter)
        
        let bakedBeans = FoodItem()
        bakedBeans.name = "Baked Beans"
        bakedBeans.calories = 105
        bakedBeans.carbohydrates = 20
        bakedBeans.protein = 5
        bakedBeans.fats = 0
        breakfastAndLunchItems.addObject(bakedBeans)
        
        let saladFruit = FoodItem()
        saladFruit.name = "Salad: Fruit"
        saladFruit.calories = 88
        saladFruit.carbohydrates = 23
        saladFruit.protein = 0
        saladFruit.fats = 0
        breakfastAndLunchItems.addObject(saladFruit)
        
        let saladVegetable = FoodItem()
        saladVegetable.name = "Salad: Vegetable (No Dressing)"
        saladVegetable.calories = 33
        saladVegetable.carbohydrates = 7
        saladVegetable.protein = 3
        saladVegetable.fats = 0
        breakfastAndLunchItems.addObject(saladVegetable)
        
        let croissant = FoodItem()
        croissant.name = "Croissant"
        croissant.calories = 406
        croissant.carbohydrates = 46
        croissant.protein = 8
        croissant.fats = 21
        breakfastAndLunchItems.addObject(croissant)
        
        let hamSmoked = FoodItem()
        hamSmoked.name = "Ham: Smoked (Pork)"
        hamSmoked.calories = 122
        hamSmoked.carbohydrates = 7
        hamSmoked.protein = 18
        hamSmoked.fats = 2
        breakfastAndLunchItems.addObject(hamSmoked)
        
        let hamBoiled = FoodItem()
        hamBoiled.name = "Ham: Boiled (Pork)"
        hamBoiled.calories = 104
        hamBoiled.carbohydrates = 1
        hamBoiled.protein = 17
        hamBoiled.fats = 4
        breakfastAndLunchItems.addObject(hamBoiled)
        
        let hamTurkey = FoodItem()
        hamTurkey.name = "Ham: Turkey (Cured)"
        hamTurkey.calories = 126
        hamTurkey.carbohydrates = 2
        hamTurkey.protein = 17
        hamTurkey.fats = 5
        breakfastAndLunchItems.addObject(hamTurkey)
        
        let honey = FoodItem()
        honey.name = "Honey"
        honey.calories = 304
        honey.carbohydrates = 82
        honey.protein = 0
        honey.fats = 0
        breakfastAndLunchItems.addObject(honey)
        
        let hashBrown = FoodItem()
        hashBrown.name = "Hash Brown"
        hashBrown.calories = 265
        hashBrown.carbohydrates = 35
        hashBrown.protein = 3
        hashBrown.fats = 13
        breakfastAndLunchItems.addObject(hashBrown)
        
        let englishMuffin = FoodItem()
        englishMuffin.name = "English Muffin"
        englishMuffin.calories = 243
        englishMuffin.carbohydrates = 49
        englishMuffin.protein = 9
        englishMuffin.fats = 2
        breakfastAndLunchItems.addObject(englishMuffin)
        
        let oats = FoodItem()
        oats.name = "Oats"
        oats.calories = 389
        oats.carbohydrates = 66
        oats.protein = 17
        oats.fats = 7
        breakfastAndLunchItems.addObject(oats)
        
        let yogurtFruit = FoodItem()
        yogurtFruit.name = "Yogurt: Fruit (Non Fat)"
        yogurtFruit.calories = 95
        yogurtFruit.carbohydrates = 19
        yogurtFruit.protein = 6
        yogurtFruit.fats = 0
        breakfastAndLunchItems.addObject(yogurtFruit)
        
        let yogurtGreek = FoodItem()
        yogurtGreek.name = "Yogurt: Greek"
        yogurtGreek.calories = 87
        yogurtGreek.carbohydrates = 3
        yogurtGreek.protein = 7
        yogurtGreek.fats = 5
        breakfastAndLunchItems.addObject(yogurtGreek)
        
        let pancakePlain = FoodItem()
        pancakePlain.name = "Pancake: Plain"
        pancakePlain.calories = 227
        pancakePlain.carbohydrates = 28
        pancakePlain.protein = 6
        pancakePlain.fats = 10
        breakfastAndLunchItems.addObject(pancakePlain)
        
        let waffle = FoodItem()
        waffle.name = "Waffle"
        waffle.calories = 312
        waffle.carbohydrates = 49
        waffle.protein = 7
        waffle.fats = 10
        breakfastAndLunchItems.addObject(waffle)
        
        let muesli = FoodItem()
        muesli.name = "Muesli"
        muesli.calories = 340
        muesli.carbohydrates = 78
        muesli.protein = 10
        muesli.fats = 5
        breakfastAndLunchItems.addObject(muesli)
        
        let soupChicken = FoodItem()
        soupChicken.name = "Soup: Chicken (Canned)"
        soupChicken.calories = 71
        soupChicken.carbohydrates = 7
        soupChicken.protein = 5
        soupChicken.fats = 3
        breakfastAndLunchItems.addObject(soupChicken)
        
        let soupTomato = FoodItem()
        soupTomato.name = "Soup: Tomato (Canned)"
        soupTomato.calories = 60
        soupTomato.carbohydrates = 13
        soupTomato.protein = 5
        soupTomato.fats = 1
        breakfastAndLunchItems.addObject(soupTomato)
        
        let milkSkimmed = FoodItem()
        milkSkimmed.name = "Milk: Skimmed"
        milkSkimmed.calories = 35
        milkSkimmed.carbohydrates = 5
        milkSkimmed.protein = 3
        milkSkimmed.fats = 0
        breakfastAndLunchItems.addObject(milkSkimmed)
        
        let milkSemiSkimmed = FoodItem()
        milkSemiSkimmed.name = "Milk: Semi-Skimmed"
        milkSemiSkimmed.calories = 47
        milkSemiSkimmed.carbohydrates = 5
        milkSemiSkimmed.protein = 4
        milkSemiSkimmed.fats = 2
        breakfastAndLunchItems.addObject(milkSemiSkimmed)
        
        let milkWhole = FoodItem()
        milkWhole.name = "Milk: Whole"
        milkWhole.calories = 60
        milkWhole.carbohydrates = 5
        milkWhole.protein = 5
        milkWhole.fats = 3
        breakfastAndLunchItems.addObject(milkWhole)
        
        
        return breakfastAndLunchItems
    }
    

    //--------------------------------------------------
    // DINNER MENU
    //--------------------------------------------------
    func populateDinnerItems() -> NSMutableArray
    {
        let dinnerItems: NSMutableArray! = NSMutableArray()

        let chickenBreast = FoodItem()
        chickenBreast.name = "Chicken Breast, Cooked"
        chickenBreast.calories = 165
        chickenBreast.carbohydrates = 0
        chickenBreast.protein = 31
        chickenBreast.fats = 4
        dinnerItems.addObject(chickenBreast)
        
        let riceWhite = FoodItem()
        riceWhite.name = "Rice (White), Cooked"
        riceWhite.calories = 130
        riceWhite.carbohydrates = 28
        riceWhite.protein = 3
        riceWhite.fats = 0
        dinnerItems.addObject(riceWhite)
        
        let riceBrown = FoodItem()
        riceBrown.name = "Rice (Brown): Cooked"
        riceBrown.calories = 111
        riceBrown.carbohydrates = 23
        riceBrown.protein = 3
        riceBrown.fats = 0
        dinnerItems.addObject(riceBrown)
        
        let mashedPotatoBoth = FoodItem()
        mashedPotatoBoth.name = "Mashed Potatoes: Milk and Butter"
        mashedPotatoBoth.calories = 113
        mashedPotatoBoth.carbohydrates = 17
        mashedPotatoBoth.protein = 2
        mashedPotatoBoth.fats = 4
        dinnerItems.addObject(mashedPotatoBoth)
        
        let mashedPotatoMilkOnly = FoodItem()
        mashedPotatoMilkOnly.name = "Mashed Potatoes: Milk Only"
        mashedPotatoMilkOnly.calories = 83
        mashedPotatoMilkOnly.carbohydrates = 18
        mashedPotatoMilkOnly.protein = 2
        mashedPotatoMilkOnly.fats = 1
        dinnerItems.addObject(mashedPotatoMilkOnly)
        
        let pasta = FoodItem()
        pasta.name = "Pasta"
        pasta.calories = 131
        pasta.carbohydrates = 25
        pasta.protein = 5
        pasta.fats = 1
        dinnerItems.addObject(pasta)
        
        let pizzaCheeseThinCrust = FoodItem()
        pizzaCheeseThinCrust.name = "Pizza: Cheese Only (Thin Crust)"
        pizzaCheeseThinCrust.calories = 258
        pizzaCheeseThinCrust.carbohydrates = 29
        pizzaCheeseThinCrust.protein = 11
        pizzaCheeseThinCrust.fats = 11
        dinnerItems.addObject(pizzaCheeseThinCrust)
        
        let tunaCooked = FoodItem()
        tunaCooked.name = "Tuna: Cooked"
        tunaCooked.calories = 184
        tunaCooked.carbohydrates = 0
        tunaCooked.protein = 30
        tunaCooked.fats = 6
        dinnerItems.addObject(tunaCooked)
        
        let lasagna = FoodItem()
        lasagna.name = "Lasagna"
        lasagna.calories = 241
        lasagna.carbohydrates = 20
        lasagna.protein = 12
        lasagna.fats = 12
        dinnerItems.addObject(lasagna)
        
        let turkeyBreast = FoodItem()
        turkeyBreast.name = "Turkey Breast"
        turkeyBreast.calories = 111
        turkeyBreast.carbohydrates = 0
        turkeyBreast.protein = 25
        turkeyBreast.fats = 1
        dinnerItems.addObject(turkeyBreast)

        let spaghettiMeatballs = FoodItem()
        spaghettiMeatballs.name = "Spaghetti Meatballs"
        spaghettiMeatballs.calories = 107
        spaghettiMeatballs.carbohydrates = 11
        spaghettiMeatballs.protein = 4
        spaghettiMeatballs.fats = 5
        dinnerItems.addObject(spaghettiMeatballs)

        let grilledChickenSalad = FoodItem()
        grilledChickenSalad.name = "Grilled Chicken Salad"
        grilledChickenSalad.calories = 110
        grilledChickenSalad.carbohydrates = 6
        grilledChickenSalad.protein = 12
        grilledChickenSalad.fats = 4
        dinnerItems.addObject(grilledChickenSalad)
        
        let beefStew = FoodItem()
        beefStew.name = "Beef Stew"
        beefStew.calories = 95
        beefStew.carbohydrates = 7
        beefStew.protein = 5
        beefStew.fats = 5
        dinnerItems.addObject(beefStew)

        let chickenNoodleSoup = FoodItem()
        chickenNoodleSoup.name = "Chicken Noodle Soup"
        chickenNoodleSoup.calories = 37
        chickenNoodleSoup.carbohydrates = 4
        chickenNoodleSoup.protein = 3
        chickenNoodleSoup.fats = 12
        dinnerItems.addObject(chickenNoodleSoup)

        let spaghettiMarinara = FoodItem()
        spaghettiMarinara.name = "Spaghetti: Marinara"
        spaghettiMarinara.calories = 87
        spaghettiMarinara.carbohydrates = 14
        spaghettiMarinara.protein = 2
        spaghettiMarinara.fats = 3
        dinnerItems.addObject(spaghettiMarinara)

        let pizzaPepperoni = FoodItem()
        pizzaPepperoni.name = "Pizza: Pepperoni Only (Thin Crust)"
        pizzaPepperoni.calories = 284
        pizzaPepperoni.carbohydrates = 24
        pizzaPepperoni.protein = 11
        pizzaPepperoni.fats = 16
        dinnerItems.addObject(pizzaPepperoni)

        let stirFry = FoodItem()
        stirFry.name = "Stir Fry"
        stirFry.calories = 101
        stirFry.carbohydrates = 21
        stirFry.protein = 9
        stirFry.fats = 0
        dinnerItems.addObject(stirFry)
        
        let frenchFries = FoodItem()
        frenchFries.name = "French Fries"
        frenchFries.calories = 276
        frenchFries.carbohydrates = 39
        frenchFries.protein = 3
        frenchFries.fats = 12
        dinnerItems.addObject(frenchFries)
        
        let potatoBoiledWithSkin = FoodItem()
        potatoBoiledWithSkin.name = "Potato Boiled: With Skin"
        potatoBoiledWithSkin.calories = 87
        potatoBoiledWithSkin.carbohydrates = 20
        potatoBoiledWithSkin.protein = 2
        potatoBoiledWithSkin.fats = 0
        dinnerItems.addObject(potatoBoiledWithSkin)
        
        let potatoBoiledNoSkin = FoodItem()
        potatoBoiledNoSkin.name = "Potato Boiled: No Skin"
        potatoBoiledNoSkin.calories = 86
        potatoBoiledNoSkin.carbohydrates = 20
        potatoBoiledNoSkin.protein = 2
        potatoBoiledNoSkin.fats = 0
        dinnerItems.addObject(potatoBoiledNoSkin)
        
        let sweetPotato = FoodItem()
        sweetPotato.name = "Sweet Potatoes"
        sweetPotato.calories = 90
        sweetPotato.carbohydrates = 21
        sweetPotato.protein = 2
        sweetPotato.fats = 0
        dinnerItems.addObject(sweetPotato)

        return dinnerItems
        
    }
    
    //--------------------------------------------------
    // SNACK MENU
    //--------------------------------------------------
    func populateSnackItems() -> NSMutableArray
    {
        let snackItems: NSMutableArray! = NSMutableArray()

        let crisps = FoodItem()
        crisps.name = "Crisps: Any Flavour"
        crisps.calories = 536
        crisps.carbohydrates = 53
        crisps.protein = 7
        crisps.fats = 35
        snackItems.addObject(crisps)
        
        let milkChocolate = FoodItem()
        milkChocolate.name = "Chocolate: Milk"
        milkChocolate.calories = 535
        milkChocolate.carbohydrates = 59
        milkChocolate.protein = 8
        milkChocolate.fats = 30
        snackItems.addObject(milkChocolate)
        
        let whiteChocolate = FoodItem()
        whiteChocolate.name = "Chocolate: White"
        whiteChocolate.calories = 539
        whiteChocolate.carbohydrates = 59
        whiteChocolate.protein = 6
        whiteChocolate.fats = 32
        snackItems.addObject(whiteChocolate)
        
        let darkChocolate = FoodItem()
        darkChocolate.name = "Chocolate: Dark"
        darkChocolate.calories = 546
        darkChocolate.carbohydrates = 61
        darkChocolate.protein = 4.9
        darkChocolate.fats = 31
        snackItems.addObject(darkChocolate)
        
        let almondNuts = FoodItem()
        almondNuts.name = "Nuts: Almonds"
        almondNuts.calories = 576
        almondNuts.carbohydrates = 22
        almondNuts.protein = 21
        almondNuts.fats = 49
        snackItems.addObject(almondNuts)
        
        let brazillianNuts = FoodItem()
        brazillianNuts.name = "Nuts: Brazillian"
        brazillianNuts.calories = 656
        brazillianNuts.carbohydrates = 12
        brazillianNuts.protein = 14
        brazillianNuts.fats = 66
        snackItems.addObject(brazillianNuts)
        
        let cashewNuts = FoodItem()
        cashewNuts.name = "Nuts: Cashews"
        cashewNuts.calories = 575
        cashewNuts.carbohydrates = 28
        cashewNuts.protein = 18
        cashewNuts.fats = 49
        snackItems.addObject(cashewNuts)
        
        let jellies = FoodItem()
        jellies.name = "Jellies"
        jellies.calories = 266
        jellies.carbohydrates = 70
        jellies.protein = 0
        jellies.fats = 0
        snackItems.addObject(jellies)
        
        let hotFudge = FoodItem()
        hotFudge.name = "Hot Fudge"
        hotFudge.calories = 180
        hotFudge.carbohydrates = 30
        hotFudge.protein = 4
        hotFudge.fats = 5
        snackItems.addObject(hotFudge)
        
        let marshmallows = FoodItem()
        marshmallows.name = "Marshmallows"
        marshmallows.calories = 318
        marshmallows.carbohydrates = 81
        marshmallows.protein = 2
        marshmallows.fats = 0
        snackItems.addObject(marshmallows)
        
        let caramels = FoodItem()
        caramels.name = "Caramels"
        caramels.calories = 382
        caramels.carbohydrates = 77
        caramels.protein = 5
        caramels.fats = 8
        snackItems.addObject(caramels)
        
        let jellyBeans = FoodItem()
        jellyBeans.name = "Jelly Beans"
        jellyBeans.calories = 375
        jellyBeans.carbohydrates = 94
        jellyBeans.protein = 0
        jellyBeans.fats = 0
        snackItems.addObject(jellyBeans)
        
        let nougat = FoodItem()
        nougat.name = "Nougat"
        nougat.calories = 398
        nougat.carbohydrates = 92
        nougat.protein = 3
        nougat.fats = 2
        snackItems.addObject(nougat)
        
        let sesameCrunch = FoodItem()
        sesameCrunch.name = "Sesame Crunch"
        sesameCrunch.calories = 516
        sesameCrunch.carbohydrates = 50
        sesameCrunch.protein = 12
        sesameCrunch.fats = 33
        snackItems.addObject(sesameCrunch)
        
        let sweetPotatoChips = FoodItem()
        sweetPotatoChips.name = "Sweet Potato Chips"
        sweetPotatoChips.calories = 496
        sweetPotatoChips.carbohydrates = 65
        sweetPotatoChips.protein = 4
        sweetPotatoChips.fats = 25
        snackItems.addObject(sweetPotatoChips)
        
        let toffee = FoodItem()
        toffee.name = "Toffee"
        toffee.calories = 560
        toffee.carbohydrates = 65
        toffee.protein = 1
        toffee.fats = 33
        snackItems.addObject(toffee)
        
        let porkStratchings = FoodItem()
        porkStratchings.name = "Pork Scratchings"
        porkStratchings.calories = 544
        porkStratchings.carbohydrates = 0
        porkStratchings.protein = 61
        porkStratchings.fats = 31
        snackItems.addObject(porkStratchings)
        
        return snackItems
    }

}
