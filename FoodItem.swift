//
//  FoodItem.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 23/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class FoodItem: NSObject {

    var name = ""
    var calories = 0.0
    var carbohydrates = 0.0
    var protein = 0.0
    var fats = 0.0
}
