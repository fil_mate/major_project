//
//  DailyStatisticsViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 02/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class DailyStatisticsViewController: UITableViewController {

    @IBOutlet var dailyStatisticsTableView: UITableView!
    var dateString = ""
    var date = NSDate()
    var healthManager: HealthKitManager?
    var nutritionStatistics = ["Calories", "Carbohydrates", "Protein", "Fats"]
    var fitnessStatistics = ["BMR", "Active burn", "Fitness burn"]
    let dailyStatistics = DailyStatistics()
    var totalCaloriesBMR = 0.0
    var totalCaloriesActiveBurn = 0.0
    var totalCaloriesWorkouts = 0.0
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = dateString
        healthManager = HealthKitManager()
    }

    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        //---------------------------------
        // 1. Reading Calories
        //---------------------------------
        var sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!
        var unitForSample = HKUnit.kilocalorieUnit()
        //---------------------------------
        healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date: date, completion: ({ (calories, caloriesError) -> Void in
        if(caloriesError != nil)
        {
            print("Error reading calories: \(caloriesError!.localizedDescription)")
            return
        }
        //---------------------------------
        // 2. Reading Carbohydrates
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)!
        unitForSample = HKUnit.gramUnit()
        self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample,date: self.date, completion: ({ (carbohydrates, carbohydratesError) -> Void in
        if(carbohydratesError != nil)
        {
            print("Error reading carbohydrates: \(carbohydratesError!.localizedDescription)")
            return
        }
        //---------------------------------
        // 3. Reading Protein
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)!
        self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date:self.date, completion: ({ (protein, proteinError) -> Void in
        if(proteinError != nil)
        {
            print("Error reading protein: \(proteinError!.localizedDescription)")
            return
        }
        //---------------------------------
        // 4. Reading Fats
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)!
        self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date: self.date, completion: ({ (fats, fatsError) -> Void in
        if(fatsError != nil)
        {
            print("Error reading fats: \(fatsError!.localizedDescription)")
            return
        }
        //---------------------------------
        // 5. Reading Distance Walked/Run
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!
        unitForSample = HKUnit.meterUnit()
        self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date: self.date, completion: ({ (distanceInMeters, distanceError) -> Void in
        if(distanceError != nil)
        {
            print("Error reading distance: \(distanceError!.localizedDescription)")
            return
        }
        //---------------------------------
        // 6. Reading Activities for day
        //---------------------------------
        self.healthManager?.readAllActivitiesByDate(self.date, completion: ({ (workouts, activitiesError) -> Void in
        if( activitiesError != nil )
        {
            print("Error reading activities: \(activitiesError.localizedDescription)")
            return
        }
        //---------------------------------
        // 7. Reading Height for BMR calculation
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!
        self.healthManager?.readMostRecentSample(sampleType, completion: ({ (mostRecentHeight, heightError) -> Void in
        if( heightError != nil )
        {
            print("Error reading height: \(heightError.localizedDescription)")
            return
        }
        //---------------------------------
        // 8. Reading Weight for BMR calculation
        //---------------------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!
        self.healthManager?.readMostRecentSample(sampleType, completion: ({ (mostRecentWeight,weightError) -> Void in
        if( weightError != nil )
        {
            print("Error reading weight: \(weightError.localizedDescription)")
            return
        }
        //---------------------------------
        // 9. Reading User Sex for BMR calculation
        //---------------------------------
        if let biologicalSexObject = self.healthManager?.readUserSex()
        {
            self.dailyStatistics.userSex = (self.healthManager?.biologicalSexDisplayName(biologicalSexObject.biologicalSex))!
        }
        //---------------------------------
        // 10. Reading User Age for BMR calculation
        //---------------------------------
        if let age = self.healthManager?.readUserAge()
        {
            self.dailyStatistics.userAge = age
        }
        //---------------------------------
        print("Statistics for " + String(self.date) + " read sucessfully")
        //---------------------------------------
        self.dailyStatistics.date = self.date
        self.dailyStatistics.totalCalories = calories
        self.dailyStatistics.totalCarbohydrates = carbohydrates
        self.dailyStatistics.totalProtein = protein
        self.dailyStatistics.totalFats = fats
        self.dailyStatistics.distanceFromWalking = distanceInMeters
        self.dailyStatistics.workouts = workouts as! [HKWorkout]
        let mostRecentHeight = mostRecentHeight as! HKQuantitySample
        let heightInMeters = mostRecentHeight.quantity.doubleValueForUnit(HKUnit.meterUnit())
        self.dailyStatistics.userHeight = heightInMeters
        let mostRecentWeight = mostRecentWeight as! HKQuantitySample;
        let weightInKilograms = mostRecentWeight.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo))
        self.dailyStatistics.userWeight = weightInKilograms
        //---------------------------------------
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
        }))
        }))
        }))
        }))
        }))
        }))    
        }))
        }))
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return nutritionStatistics.count
        }
        else if (section == 1)
        {
            return fitnessStatistics.count
        }
        else
        {
            return 1
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = dailyStatisticsTableView.dequeueReusableCellWithIdentifier("StatisticCell", forIndexPath: indexPath)
        if (indexPath.section == 0)
        {
            let titleText = nutritionStatistics[indexPath.row]
            cell.textLabel?.text = titleText
            var detailText = "Unknown"
            if (titleText == "Calories")
            {
                var calories = dailyStatistics.totalCalories
                calories = Double(round(10*calories)/10)
                detailText = String(calories) + " kcal"
            }
            else if (titleText == "Carbohydrates")
            {
                var carbohydrates = dailyStatistics.totalCarbohydrates
                carbohydrates = Double(round(10*carbohydrates)/10)
                detailText = String(carbohydrates) + " g"
            }
            else if (titleText == "Protein")
            {
                var protein = dailyStatistics.totalProtein
                protein = Double(round(10*protein)/10)
                detailText = String(protein) + " g"
            }
            else
            {
                var fats = dailyStatistics.totalFats
                fats = Double(round(10*fats)/10)
                detailText = String(fats) + " g"
            }
            cell.detailTextLabel?.text = detailText
        }
        else if (indexPath.section == 1)
        {
            let titleText = fitnessStatistics[indexPath.row]
            cell.textLabel?.text = titleText
            var detailText = "Unknown"
            let userWeight = dailyStatistics.userWeight
            let userHeight = dailyStatistics.userHeight
            let userAge = dailyStatistics.userAge
            let userSex = dailyStatistics.userSex
            var userBMR = 0.0
            if (userSex == "Male")
            {
                // Equation split in two because of Swift compiler error
                let calculationPartOne = 10 * userWeight
                let calculationPartTwo = calculationPartOne + 6.25 * (Double(userHeight*100)) - 5 * Double(userAge) + 5
                userBMR = calculationPartTwo
                totalCaloriesBMR = userBMR
            }
            if (userSex == "Female")
            {
                let calculationPartOne = 10 * userWeight
                let calculationPartTwo = calculationPartOne + 6.25 * (Double(userHeight*100)) - 5 * Double(userAge) - 161
                userBMR = calculationPartTwo
                totalCaloriesBMR = userBMR
            }
            //--------------------------------------
            if (titleText == "BMR")
            {
                detailText = String(userBMR)
            }
            else if(titleText == "Active burn")
            {
                let distanceActive = (dailyStatistics.distanceFromWalking)/1000
                let walkSpeed = 4.8
                let timeHours = distanceActive/walkSpeed
                var activeCalorieBurn =  userBMR * 3.5/24 * timeHours
                activeCalorieBurn = Double(round(10*activeCalorieBurn)/10)
                totalCaloriesActiveBurn = activeCalorieBurn
                detailText = String(activeCalorieBurn)
            }
            else
            {
                var totalEnergyFromWorkouts = 0.0
                for workout in dailyStatistics.workouts
                {
                    if (workout.totalEnergyBurned != nil)
                    {
                        let energyBurned = workout.totalEnergyBurned!.doubleValueForUnit(HKUnit.kilocalorieUnit())
                        totalEnergyFromWorkouts += energyBurned
                    }
                }
                totalCaloriesWorkouts = totalEnergyFromWorkouts
                detailText = String(totalEnergyFromWorkouts)
            }
            cell.detailTextLabel?.text = detailText + " kcal"
        }
        else
        {
            let titleText = "Total"
            cell.textLabel?.text = titleText
            var netCalories = dailyStatistics.totalCalories - (totalCaloriesBMR + totalCaloriesActiveBurn + totalCaloriesWorkouts)
            netCalories = Double(round(10*netCalories)/10)
            cell.detailTextLabel?.text = String(netCalories) + " kcal"

        }
        //----------
        return cell
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("titleCell")
        
        headerCell!.backgroundColor = UIColor(hue: 161/360, saturation: 59/100, brightness: 90/100, alpha: 0.2)
        
        if (section == 0)
        {
            headerCell?.textLabel?.text = "Nutrition Intake"
            
        }
        else if (section == 1)
        {
            headerCell!.textLabel?.text = "Fitness Statistics"
        }
        else
        {
            headerCell!.textLabel?.text = "Net Caloric Value"
        }
        return headerCell
    }

}
