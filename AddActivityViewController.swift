//
//  AddActivityViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 05/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//
import HealthKit
import UIKit

class AddActivityViewController: UIViewController {

    var screenTitle: String!
    
    @IBOutlet var activitiesDatePicker: UIDatePicker!
    @IBOutlet var dateField: UITextField!
    @IBOutlet var startTimeField: UITextField!
    @IBOutlet var endTimeField: UITextField!
    @IBOutlet var distanceField: UITextField?
    @IBOutlet var caloriesField: UITextField?
    var healthManager: HealthKitManager?
    var fitnessItems: FitnessActivities = FitnessActivities()
    let dateFormatter = NSDateFormatter()
    var universal = FitmateUniversal()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = screenTitle
        healthManager = HealthKitManager()
        activitiesDatePicker.hidden = true
        distanceField!.keyboardType = UIKeyboardType.DecimalPad
        caloriesField!.keyboardType = UIKeyboardType.DecimalPad
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func doSelectDate(sender: AnyObject)
    {
        dateField.inputView = UIView() // disables the default keyboard
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        activitiesDatePicker.hidden = false
        activitiesDatePicker.datePickerMode = .Date
    }
    
    @IBAction func doSelectStartTime(sender: AnyObject)
    {
        startTimeField.inputView = UIView()// disables the default keyboard
        
        dateFormatter.dateFormat = "HH:mm"
        startTimeField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        activitiesDatePicker.hidden = false
        activitiesDatePicker.datePickerMode = .Time
    }
        
    @IBAction func doSelectEndTime(sender: AnyObject)
    {
        endTimeField.inputView = UIView()// disables the default keyboard

        dateFormatter.dateFormat = "HH:mm"
        endTimeField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        activitiesDatePicker.hidden = false
        activitiesDatePicker.datePickerMode = .Time
    }
    
    @IBAction func doSelectDistance(sender: AnyObject)
    {
        activitiesDatePicker.hidden = true
    }
    
    @IBAction func doSelectCalories(sender: AnyObject)
    {
        activitiesDatePicker.hidden = true
    }
    
    @IBAction func doActivityDatePicker(sender: AnyObject)
    {
        if dateField.isFirstResponder()
        {
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        }
        else if startTimeField.isFirstResponder()
        {
            dateFormatter.dateFormat = "HH:mm"
            startTimeField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        }
        else if endTimeField.isFirstResponder()
        {
            dateFormatter.dateFormat = "HH:mm"
            endTimeField.text! = dateFormatter.stringFromDate(activitiesDatePicker.date)
        }
    }
    
    var distance:Double?
    {
        return Double(distanceField!.text!)
    }
    
    var calories:Double?
    {
        return Double(caloriesField!.text!)
    }
    
    @IBAction func doSaveActivity(sender: AnyObject)
    {
        //Error handling
        //If any of the first three are emptys
        if (dateField.hasText() == false || startTimeField.hasText() == false || endTimeField.hasText() == false)
        {
            let alertView = UIAlertController(title: "Error", message: "Please type in Date, Start Time and End Time", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        //Error handling
        //If End time is before start time
        if (startTimeField.text >= endTimeField.text)
        {
            let alertView = UIAlertController(title: "Error", message: "Please type in End Time which is after Start Time", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        let hkUnit = HKUnit.meterUnitWithMetricPrefix(.Kilo)

        var dayDate = NSDate()
        dayDate = universal.convertDate(dateField.text!)
        //------------------------------------
        var startTime = NSDate()
        startTime = universal.convertTime(startTimeField.text!)
        //------------------------------------
        var endTime = NSDate()
        endTime = universal.convertTime(endTimeField.text!)
        //------------------------------------
        let fullStartTime = universal.combineTimeWithDate(dayDate, time: startTime)
        let fullEndtime = universal.combineTimeWithDate(dayDate, time: endTime)
        //------------------------------------
        let activityType = fitnessItems.getActivityType(self.title!)
        //------------------------------------
        healthManager?.saveWorkout(activityType, startDate: fullStartTime!, endDate: fullEndtime!, distance: distance, distanceUnit: hkUnit, calories: calories, completion: { (success, error ) -> Void in
            if( success )
            {
                print("Workout saved!")
            }
            else if( error != nil ) {
                print("\(error)")
            }
        })
    }
}
