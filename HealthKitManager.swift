//
//  HealthKitManager.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 24/02/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import Foundation
import HealthKit


class HealthKitManager: NSObject {
    
    var userAge = 0
    
    let healthKitStore:HKHealthStore = HKHealthStore()
    
    //-------------------------------------
    // Authorisation code based on the code from the tutorial on Ray Wenderlich as referenced in the report
    //-------------------------------------
    func authoriseWithHealthKit (completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        // First check if HealthKit is available
        if ( !HKHealthStore.isHealthDataAvailable() )
        {
            let error = NSError(domain: "fik.Fitmate", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return
        }
        
        // Set info to READ from HealthKit
        let typesToRead = Set([
            
            // Characteristic Types
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
            HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,
            // Quantity Types
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
            HKObjectType.workoutType()
            ])
        
        // Set info to WRITE to HealthKit
        let typesToWrite = Set([
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,

            HKQuantityType.workoutType()
            ])
        
        // Request Authorisation with based on the read and write types
        healthKitStore.requestAuthorizationToShareTypes(typesToWrite, readTypes: typesToRead) { (success, error) -> Void in
            
            if( completion != nil )
            {
                completion(success:success,error:error)
            }
        }
    }
    
    //--------------------------
    // This function reads in the user age from HK
    // Based on RW tutorial
    //--------------------------
    func readUserAge() -> Int?
    {
        var age:Int?
        var birthDay:NSDate
        do{
            birthDay = try healthKitStore.dateOfBirth()
            //-------------
            let today = NSDate()
            let yearDifference = NSCalendar.currentCalendar().components(.Year, fromDate: birthDay, toDate: today, options: NSCalendarOptions(rawValue: 0) )
            age = yearDifference.year
        } catch let error as NSError {
            print( error.domain )
        }
        //------------------
        return age
    }
    
    
    //--------------------------------------
    // Read biological sex
    // Based on RW tutorial
    //--------------------------------------
    func readUserSex() -> HKBiologicalSexObject?
    {
        var biologicalSex:HKBiologicalSexObject?
        // Try to get the sex from HK, if unsuccesful, throw and print error
        do{
            biologicalSex = try healthKitStore.biologicalSex()
        } catch let error as NSError {
            print( error.domain )
        }
        //------------------
        return biologicalSex
    }
    
    //--------------------------------------
    // Need this method to convert the biological sex type to a String name
    //--------------------------------------
    func biologicalSexDisplayName(biologicalSex:HKBiologicalSex?)->String
    {
        var biologicalSexText = "Unknown"
        if  biologicalSex == nil
        {
            return biologicalSexText
        }
            
        else
        {
            //Handled by switch case as the API uses an enum
            switch( biologicalSex! )
            {
            case .Female:
                biologicalSexText = "Female"
                
            case .Male:
                biologicalSexText = "Male"
                
            case .NotSet:
                biologicalSexText = "Not Set"
                
            default:
                break
            }
        }
        return biologicalSexText
    }
    
    //--------------------------------
    // Based on the code from the RW tutorial
    // This is different from the Characteristic because it's a Sample
    // It requires a separate method to get the most recent sample from HK
    // The method reads in the most recent HeakthKit sample from a list of samples in the database
    //--------------------------------
    func readMostRecentSample(sampleType:HKSampleType , completion: ((HKSample!, NSError!) -> Void)!)
    {
        let past = NSDate.distantPast()
        let now = NSDate()
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: 1, sortDescriptors: [sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                
                if let _ = error {
                    completion(nil,error)
                    return
                }
                // Get the first sample
                let mostRecentSample = results!.first as? HKQuantitySample
                if completion != nil {
                    completion(mostRecentSample,nil)
                }
        }
        //-----------------------
        self.healthKitStore.executeQuery(sampleQuery)
    }
    
    func saveWorkout(activityType: HKWorkoutActivityType, startDate:NSDate , endDate:NSDate , distance:Double?, distanceUnit:HKUnit , calories:Double?, completion: ( (Bool, NSError!) -> Void)!)
    {

        // First create HKQuantity with distance as 0
        var hKDistance = HKQuantity(unit: distanceUnit, doubleValue: 0)
        if (distance != nil)
        {
            hKDistance = HKQuantity(unit: distanceUnit, doubleValue: distance!)
        }
        
        var hKCalories = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: 0)
        if (calories != nil)
        {
            hKCalories = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: calories!)
        }
    
        //Create workout with the distance and calories as passed in by the user
        var workout = HKWorkout(activityType: activityType, startDate: startDate, endDate: endDate, duration: abs(endDate.timeIntervalSinceDate(startDate)), totalEnergyBurned: hKCalories, totalDistance: hKDistance, metadata: nil)
        
        // If user hasn't typed in the distance
        if (distance == nil)
        {
            workout = HKWorkout(activityType: activityType, startDate: startDate, endDate: endDate, duration: abs(endDate.timeIntervalSinceDate(startDate)), totalEnergyBurned: hKCalories, totalDistance: nil, metadata: nil)
        }
        
        //If user hasn't typed in the calories
        if (calories == nil)
        {
            workout = HKWorkout(activityType: activityType, startDate: startDate, endDate: endDate, duration: abs(endDate.timeIntervalSinceDate(startDate)), totalEnergyBurned: nil, totalDistance: hKDistance, metadata: nil)
        }
        
        //If user hasn't typed in the distance and calories
        if (distance == nil && calories == nil)
        {
            workout = HKWorkout(activityType: activityType, startDate: startDate, endDate: endDate, duration: abs(endDate.timeIntervalSinceDate(startDate)), totalEnergyBurned: nil, totalDistance: nil, metadata: nil)
        }
        
        healthKitStore.saveObject(workout, withCompletion: { (success, error) -> Void in
            if( error != nil  ) {
                // Error saving the workout
                completion(success,error)
            }
            else {
                // Workout saved
                completion(success,nil)
                }
        })
    }
    
    func readAllActivities(completion: (([AnyObject]!, NSError!) -> Void)!)
    {
        // Build predicate
        // Show all workouts with duration less than 100000. 100000 chosen as an extrelemy high value which is equal to roughly 27 hours
        let predicateOperatorType = NSPredicateOperatorType.LessThanPredicateOperatorType
        let duration = NSTimeInterval.abs(100000)
        let predicate = HKQuery.predicateForWorkoutsWithOperatorType(predicateOperatorType, duration: duration)
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: HKWorkoutType.workoutType(), predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor])
            { (sampleQuery, results, error ) -> Void in
                
                if let queryError = error {
                    print( "Error when reading in Activities: \(queryError.localizedDescription)")
                }
                completion(results,error)
        }
        healthKitStore.executeQuery(sampleQuery)
    }
    
    //-----------------------------------
    // This method reads activities for one day only. The relevant date is passed into the method
    //-----------------------------------
    func readAllActivitiesByDate(date: NSDate, completion: (([AnyObject]!, NSError!) -> Void)!)
    {
        let calendar = NSCalendar.currentCalendar()
        let normalisedDate = calendar.startOfDayForDate(date)
        let nextDay = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: normalisedDate, options: NSCalendarOptions.MatchNextTime)!
        
        let predicate = HKQuery.predicateForSamplesWithStartDate(normalisedDate, endDate: nextDay, options: HKQueryOptions.None)
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: HKWorkoutType.workoutType(), predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                print( "Error when reading in Activities: \(queryError.localizedDescription)")
            }
            completion(results,error)
        }
        healthKitStore.executeQuery(sampleQuery)
        
    }
    
    //-----------------------------------
    // This method reads nutrition samples for one day only. The relevant date is passed into the method.
    //-----------------------------------
    func readAllNutritionByDate(sampleType: HKQuantityType, date: NSDate, completion: (([AnyObject]!, NSError!) -> Void)!)
    {
        let calendar = NSCalendar.currentCalendar()
        let normalisedDate = calendar.startOfDayForDate(date)
        let nextDay = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: normalisedDate, options: NSCalendarOptions.MatchNextTime)!
        let predicate = HKQuery.predicateForSamplesWithStartDate(normalisedDate, endDate: nextDay, options: HKQueryOptions.None)
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor])
        { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                print( "Error when reading in Nutrition by date: \(queryError.localizedDescription)")
            }
            completion(results,error)
        }
        healthKitStore.executeQuery(sampleQuery)
    }
    
    //-----------------------------------
    // This method is responsible for building a predicate necessary to read nutrition samples for one day only.
    //-----------------------------------
    func predicateForSamplesForGivenDay(givenDay: NSDate) -> NSPredicate
    {
        let calendar = NSCalendar.currentCalendar()
        let startDate = givenDay
        let endDate = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: startDate, options: NSCalendarOptions())!
        let predicate = HKQuery.predicateForSamplesWithStartDate(startDate, endDate: endDate, options: HKQueryOptions.StrictStartDate)
        //--------------
        return predicate
    }
    
    //-----------------------------------
    // This method is responsible for getting the sum of nutrition samples for a given day. It makes use of the predicateForSamplesForGivenDay method
    //-----------------------------------
    func getSumOfSamplesForGivenDate(sampleType: HKQuantityType, unit: HKUnit, date: NSDate, completion: ((Double, NSError?) -> Void)?)
    {
        let predicate = predicateForSamplesForGivenDay(date)
        let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: HKStatisticsOptions.CumulativeSum) {
            (_query, result, error) -> Void in
            let sum: HKQuantity? = result!.sumQuantity()
            if completion != nil
            {
                let value: Double = (sum != nil) ? sum!.doubleValueForUnit(unit) : 0
                completion!(value, error)
            }
        }
        healthKitStore.executeQuery(query)
    }
    
    func saveCalories(calories: Double, date: NSDate, completion: ( (Bool, NSError!) -> Void)!)
    {
        let energyConsumed = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: calories)
        let energyConsumedType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
        let energyConsumedSample = HKQuantitySample(type: energyConsumedType!, quantity: energyConsumed, startDate: date, endDate: date)
        
        healthKitStore.saveObject(energyConsumedSample, withCompletion: { (success, error) -> Void in
            if( error != nil  ) {
                // Error saving the calories
                completion(success,error)
            }
            else {
                // Calories saved
                completion(success,nil)
            }
        })
    }
    
    //-----------------------------------
    // This deletes nutritonal samples from HK Store. 
    // Samples are first read using the readAllNutritionByDate method. This is necessary to locate the correct sample.
    //-----------------------------------
    func deleteNutritionSample(sampleType: HKQuantityType, date: NSDate, index: Int, completion: ( (Bool, NSError!) -> Void)!)
    {
        self.readAllNutritionByDate(sampleType, date: date, completion: ({ (nutrition, activitiesError) -> Void in
            if( activitiesError != nil )
            {
                print("Error reading nutrition: \(activitiesError.localizedDescription)")
                return
            }
            let nutritionToDelete = nutrition as! [HKObject]
            let nutritionObjectToDelete = nutritionToDelete[index]
            self.healthKitStore.deleteObject(nutritionObjectToDelete, withCompletion: { (success, error) -> Void in
                if( error != nil  ) {
                    
                    completion(success,error)
                }
                else {
                    
                    completion(success,nil)
                }
            })
            
        }))
    }
    
    //-----------------------------------
    // This deletes fitness activities from HK Store.
    // Samples are first read using the readAllActivities method. This is necessary to locate the correct activity.
    //-----------------------------------
    func deleteWorkoutSample(index: Int, completion: ( (Bool, NSError!) -> Void)!)
    {
        self.readAllActivities({ (workouts, activitiesError) -> Void in
            if( activitiesError != nil )
            {
                print("Error reading workouts: \(activitiesError.localizedDescription)")
                return
            }
            let workoutsToDelete = workouts as! [HKWorkout]
            let workoutObjectToDelete = workoutsToDelete[index]
            self.healthKitStore.deleteObject(workoutObjectToDelete, withCompletion: { (success, error) -> Void in
                if( error != nil  ) {
                    
                    completion(success,error)
                }
                else {
                    
                    completion(success,nil)
                }
            })
            
        })
    }
    
    func saveUserWeight(weight: Double, date: NSDate, completion: ( (Bool, NSError!) -> Void)!)
    {
        let weightQuantity = HKQuantity(unit: HKUnit.gramUnitWithMetricPrefix(.Kilo), doubleValue: weight)
        let weightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        let weightSample = HKQuantitySample(type: weightType!, quantity: weightQuantity, startDate: date, endDate: date)
        
        healthKitStore.saveObject(weightSample, withCompletion: { (success, error) -> Void in
            if( error != nil  )
            {
                // Error saving the weight
                completion(success,error)
            }
            else {
                // Weight saved
                completion(success,nil)
            }
        })
    }
    
    func saveUserHeight(height: Double, date: NSDate, completion: ( (Bool, NSError!) -> Void)!)
    {
        let heightQuantity = HKQuantity(unit: HKUnit.meterUnit(), doubleValue: height)
        let heightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)
        let heightSample = HKQuantitySample(type: heightType!, quantity: heightQuantity, startDate: date, endDate: date)
        
        healthKitStore.saveObject(heightSample, withCompletion: { (success, error) -> Void in
            if( error != nil  )
            {
                // Error saving the height
                completion(success,error)
            }
            else {
                // Height saved
                completion(success,nil)
            }
        })
    }

    func saveMacros(carbohydrates: Double?, protein: Double?, fats: Double?, date: NSDate, completion: ( (Bool, NSError!) -> Void)!)
    {
        var macros = [HKObject]()
        //Carbohydrates
        if (carbohydrates != nil)
        {
            let carbohydratesConsumed = HKQuantity(unit: HKUnit.gramUnit(), doubleValue: carbohydrates!)
            let carbohydratesConsumedType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)
            let carbohydratesConsumedSample = HKQuantitySample(type: carbohydratesConsumedType!, quantity: carbohydratesConsumed, startDate: date, endDate: date)
            macros.append(carbohydratesConsumedSample)
        }
        
        //Protein
        if (protein != nil)
        {
            let proteinConsumed = HKQuantity(unit: HKUnit.gramUnit(), doubleValue: protein!)
            let proteinConsumedType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)
            let proteinConsumedSample = HKQuantitySample(type: proteinConsumedType!, quantity: proteinConsumed, startDate: date, endDate: date)
            macros.append(proteinConsumedSample)
        }
        
        //Fats
        if (fats != nil)
        {
            let fatsConsumed = HKQuantity(unit: HKUnit.gramUnit(), doubleValue: fats!)
            let fatsConsumedType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)
            let fatsConsumedSample = HKQuantitySample(type: fatsConsumedType!, quantity: fatsConsumed, startDate: date, endDate: date)
            macros.append(fatsConsumedSample)
        }
        healthKitStore.saveObjects(macros, withCompletion: { (success, error) -> Void in
            if( error != nil  ) {
                // Error saving the macros
                completion(success,error)
            }
            else {
                // Macros saved
                completion(success,nil)
            }
        })
    }
}
