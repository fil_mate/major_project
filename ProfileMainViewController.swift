//
//  ProfileMainViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 04/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class ProfileMainViewController: UITableViewController {

    var healthManager: HealthKitManager?
    var userInfo = ["Age", "Sex", "Weight", "Height"]
    var userStatistics = ["BMI", "BMR", "Body Fat", "Lean Body Mass"]
    @IBOutlet var profileTableView: UITableView!
    let userInformation = UserInformation()
    var userBMI = 0.0
    var userBodyFatPerc = 0.0

    override func viewDidLoad()
    {
        healthManager = HealthKitManager()
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        
        super.viewDidAppear(animated)
        //-------------------
        // This simple syntax below is based on the tutorial on Ray Wenderlich website. The whole reference is provided in the report document.
        //-------------------
        // 1. Age
        //-------------------
        healthManager!.authoriseWithHealthKit{ (authorized,  error) -> Void in
            if authorized {
        if let age = self.healthManager!.readUserAge()
        {
            self.userInformation.userAge = age
        }
        //-------------------
        // 2. Sex
        //-------------------
        if let biologicalSexObject = self.healthManager?.readUserSex()
        {
            self.userInformation.userSex = (self.healthManager?.biologicalSexDisplayName(biologicalSexObject.biologicalSex))!
        }
        //-------------------
        // 3. Height
        //-------------------
        var sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!
        self.healthManager?.readMostRecentSample(sampleType, completion: ({ (mostRecentHeight, heightError) -> Void in
        if( heightError != nil )
        {
            print("Error reading height from HealthKit: \(heightError.localizedDescription)")
            return
        }
        let mostRecentHeight = mostRecentHeight as? HKQuantitySample
        if (mostRecentHeight != nil)
        {
            let meters = mostRecentHeight?.quantity.doubleValueForUnit(HKUnit.meterUnit())
            self.userInformation.userHeight = Double(round(100*meters!)/100)
        }
        //-------------------
        // 4. Weight
        //-------------------
        sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!
        self.healthManager?.readMostRecentSample(sampleType, completion: ({ (mostRecentWeight, weightError) -> Void in
        if( weightError != nil )
        {
            print("Error reading height from HealthKit: \(weightError.localizedDescription)")
            return
        }
        let mostRecentWeight = mostRecentWeight as? HKQuantitySample;
        if (mostRecentWeight != nil)
        {
            let kilograms = mostRecentWeight?.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo))
            self.userInformation.userWeight = Double(round(1000*kilograms!)/1000)
        }
        //---------------------------------------
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
        }))
        }))
        }
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return userInfo.count
        }
        else
        {
            return userStatistics.count
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("profileHeadingCell")
        
        headerCell!.backgroundColor = UIColor(hue: 161/360, saturation: 59/100, brightness: 90/100, alpha: 0.2)
        //Depending on the section in the table, return relevant information
        if (section == 0)
        {
            headerCell?.textLabel!.text = "User Information"
            
        }
        else
        {
            headerCell?.textLabel!.text = "Current Statistics"
            
        }
        return headerCell
    }
    
    //-------------------
    // Here we show user information and calculate and show the user statistics
    //-------------------
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = profileTableView.dequeueReusableCellWithIdentifier("profileInfoCell", forIndexPath: indexPath)
        let userWeight = userInformation.userWeight
        let userHeight = userInformation.userHeight
        let userAge = userInformation.userAge
        let userSex = userInformation.userSex
        var userBMR = 0.0
        var detailText = "Unknown"
        if (indexPath.section == 0)
        {
            let titleText = userInfo[indexPath.row]
            cell.textLabel?.text = titleText
            if (titleText == "Age")
            {
                if (userAge == 0)
                {
                    detailText = "Not Set"
                }
                else
                {
                    let age = String(userAge)
                    detailText = age
                }
            }
            else if (titleText == "Sex")
            {
                detailText = userSex
            }
            else if (titleText == "Weight")
            {
                if (userWeight == 0.0)
                {
                    detailText = "Please Enter"
                }
                else
                {
                    let weight = String(userWeight)
                    detailText = weight + " kg"
                }
            }
            else
            {
                if (userHeight == 0.0)
                {
                    detailText = "Please Enter"
                }
                else
                {
                    let height = String(userHeight)
                    detailText = height + " m"
                }
            }
            cell.detailTextLabel!.text = detailText
        }
        else
        {
            let titleText = userStatistics[indexPath.row]
            cell.textLabel?.text = titleText
            if (titleText == "BMI")
            {
                let bMIBeforeRounding = (userWeight/(userHeight*userHeight))
                userBMI = Double(round(100*bMIBeforeRounding)/100)
                if (userBMI.isNaN || userBMI.isSignMinus || userBMI.isZero || userBMI.isInfinite)
                {
                    detailText = "Please Update Profile"
                }
                else
                {
                    detailText = String(userBMI)
                }
            }
            else if(titleText == "BMR")
            {
                if (userWeight == 0.0 || userHeight == 0.0 )
                {
                    detailText = "Please Update Profile"
                }
                // Mifflin St. Jeor Equation
                else
                {
                    if (userSex == "Male")
                    {
                        let calculationPartOne = 10 * userWeight
                        let calculationPartTwo = calculationPartOne + 6.25 * (Double(userHeight*100)) - 5 * Double(userAge) + 5
                        userBMR = calculationPartTwo
                    }
                    if (userSex == "Female")
                    {
                        let calculationPartOne = 10 * userWeight
                        let calculationPartTwo = calculationPartOne + 6.25 * (Double(userHeight*100)) - 5 * Double(userAge) - 161
                        userBMR = calculationPartTwo
                    }
                    if (userBMR.isNaN || userBMR.isSignMinus || userBMR.isZero || userBMR.isInfinite)
                    {
                        detailText = "Please Update Profile"
                    }
                    else
                    {
                        detailText = String(userBMR) + " kcal"
                    }
                }
            }
            else if (titleText == "Body Fat")
            {
                if (userSex == "Male")
                {
                    let userBFBeforeRounding = (1.20 * userBMI)+(0.23 * Double(userAge)) - 10.8 - 5.4
                    userBodyFatPerc = Double(round(100*userBFBeforeRounding)/100)
                }
                else if (userSex == "Female")
                {
                    let userBFBeforeRounding = (1.20 * userBMI)+(0.23 * Double(userAge)) - 5.4
                    userBodyFatPerc = Double(round(100*userBFBeforeRounding)/100)
                }
                if (userBodyFatPerc.isNaN || userBodyFatPerc.isSignMinus || userBodyFatPerc.isZero || userBodyFatPerc.isInfinite)
                {
                    detailText = "Please Update Profile"
                }
                else
                {
                    detailText = String(userBodyFatPerc) + " %"
                }
            }
            else
            {
                let leanBM = userWeight - ((userBodyFatPerc/100) * userWeight)
                let userLeanBodyMass = Double(round(10*leanBM)/10)
                if (userLeanBodyMass.isNaN || userLeanBodyMass.isSignMinus || userLeanBodyMass.isZero || userLeanBodyMass.isInfinite)
                {
                    detailText = "Please Update Profile"
                }
                else
                {
                    detailText = String(userLeanBodyMass) + " kg"
                }
            }
            cell.detailTextLabel?.text = detailText
        }
        
        //----------
        return cell
    }
    
    //---------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showUserInfoEditView")
        {
            let upcoming: EditUserInfoViewController = segue.destinationViewController as! EditUserInfoViewController
            var titleString = ""
            let indexPath = profileTableView.indexPathForSelectedRow
            if (indexPath!.section == 0)
            {
                titleString = userInfo[indexPath!.row]
            }
            else
            {
                titleString = userStatistics[indexPath!.row]
            }
            
            upcoming.screenTitle = titleString
            profileTableView.deselectRowAtIndexPath(indexPath!, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showUserInfoEditView", sender: self)
    }
    
    @IBAction func unwindToProfileMain(segue: UIStoryboardSegue)
    {
        let editInfoViewController = segue.sourceViewController as? EditUserInfoViewController
        editInfoViewController!.doSaveUserInfo(editInfoViewController!)
    }
}
