//
//  DiaryMainViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 02/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class DiaryMainViewController: UITableViewController {

    @IBOutlet var diaryDatesTableView: UITableView!
    var datesForSamples = [NSDate]()
    let dateFormatter = NSDateFormatter()
    var dateToday = NSDate()
    let calendar = NSCalendar.currentCalendar()
    let universal = FitmateUniversal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Diary"
        let firstDate = (NSUserDefaults.standardUserDefaults().objectForKey("firstLaunchDate") as! NSDate)
        self.datesForSamples = universal.getDatesInBetween(firstDate, endDate: dateToday)
        self.datesForSamples.sortInPlace({ $0.compare($1) == .OrderedDescending })
    }

    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datesForSamples.count
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showDailyStatsView")
        {
            // Create new DailyStatisticsViewController based on which date is pushed on with the segue
            let upcoming: DailyStatisticsViewController = segue.destinationViewController as! DailyStatisticsViewController
            let indexPath = diaryDatesTableView.indexPathForSelectedRow!
            let date = datesForSamples[indexPath.row]
            var dateString = dateFormatter.stringFromDate(date)
            if (calendar.isDate(date, inSameDayAsDate: dateToday))
            {
                dateString = "Today"
            }
            upcoming.dateString = dateString
            upcoming.date = date
            diaryDatesTableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showDailyStatsView", sender: self)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        dateFormatter.dateStyle = .FullStyle
        let cell = tableView.dequeueReusableCellWithIdentifier("DiaryCell", forIndexPath: indexPath)
        let diaryDate  = datesForSamples[indexPath.row]
        var diaryDateString = dateFormatter.stringFromDate(diaryDate)
        if (calendar.isDate(diaryDate, inSameDayAsDate: dateToday))
        {
            diaryDateString = "Today"
        }
        cell.textLabel!.text = diaryDateString
        return cell
    }
}
