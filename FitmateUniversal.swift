//
//  FitmateUniversal.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 11/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

// This class holds the universal methods used throughout the application

class FitmateUniversal: NSObject {

    let dateFormatter = NSDateFormatter()
    let calendar = NSCalendar.currentCalendar()

    //----------------------------------
    // From GitHub
    // gist.github.com/justinmfischer/0a6edf711569854c2537
    //----------------------------------
    func combineTimeWithDate(date: NSDate, time: NSDate) -> NSDate?
    {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = calendar.components([.Year, .Month, .Day], fromDate: date)
        let timeComponents = calendar.components([.Hour, .Minute], fromDate: time)
        let mergedComponments = NSDateComponents()
        mergedComponments.year = dateComponents.year
        mergedComponments.month = dateComponents.month
        mergedComponments.day = dateComponents.day
        mergedComponments.hour = timeComponents.hour
        mergedComponments.minute = timeComponents.minute
        
        return calendar.dateFromComponents(mergedComponments)
    }
    
    // -----------------------------------
    // Convert date from String to NSDate
    //------------------------------------
    func convertDate(date: String) -> NSDate
    {
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let convertedDate = dateFormatter.dateFromString(date)
        return convertedDate!
    }
    
    // -----------------------------------
    // Convert time from String to NSDate
    //------------------------------------
    func convertTime(time: String) -> NSDate
    {
        dateFormatter.dateFormat = "HH:mm"
        let convertedTime = dateFormatter.dateFromString(time)
        return convertedTime!
    }
    
    // From stackoverflow.com/questions/4895967/array-for-dates-between-two-dates
    // changed to my needs
    func getDatesInBetween(startDate: NSDate, endDate: NSDate) -> [NSDate]
    {
        let normalisedStartDate = calendar.startOfDayForDate(startDate)
        let normalisedEndDate = calendar.startOfDayForDate(endDate)
        
        var currentDay = normalisedStartDate
        var datesInBetween = [currentDay]
        if (!normalisedStartDate.isEqualToDate(normalisedEndDate))
        {
            repeat
            {
                currentDay = calendar.dateByAddingUnit(NSCalendarUnit.Day, value: 1, toDate: currentDay, options: NSCalendarOptions.MatchNextTime)!
                datesInBetween.append(currentDay)
                
            } while !calendar.isDate(currentDay, inSameDayAsDate: normalisedEndDate)
        }
        //--------------------
        return datesInBetween
    }
}
