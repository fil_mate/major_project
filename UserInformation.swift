//
//  UserInformation.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 04/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class UserInformation: NSObject{
    
    var userSex = ""
    var userAge = 0
    var userWeight = 0.0
    var userHeight = 0.0
}
