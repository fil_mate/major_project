//
//  NutritionSamplesViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 04/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class NutritionSamplesViewController: UITableViewController {

    var screenTitle = ""
    var dateForSamples = NSDate()
    var healthManager: HealthKitManager?
    var nutritionSamples = [AnyObject]()
    @IBOutlet var nutritionSamplesTableView: UITableView!
    var sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)
    var unitForSample = HKUnit.kilocalorieUnit()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = screenTitle
        healthManager = HealthKitManager()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if (screenTitle == "Calories")
        {
            self.healthManager?.readAllNutritionByDate(sampleType!, date: self.dateForSamples, completion: ({ (nutrition, nutritionError) -> Void in
                if( nutritionError != nil )
                {
                    print("Error reading Calories: \(nutritionError.localizedDescription)")
                    return
                }
                self.nutritionSamples = nutrition
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            }))
        }
        else if (screenTitle == "Carbohydrates")
        {
            let sampleTypeCarbohydrates = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)
            unitForSample = HKUnit.gramUnit()
            sampleType = sampleTypeCarbohydrates
            self.healthManager?.readAllNutritionByDate(sampleType!, date: self.dateForSamples, completion: ({ (nutrition, nutritionError) -> Void in
                if( nutritionError != nil )
                {
                    print("Error reading carbohydrates: \(nutritionError.localizedDescription)")
                    return
                }
                self.nutritionSamples = nutrition
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            }))
        }
        else if (screenTitle == "Protein")
        {
            let sampleTypeProtein = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)
            unitForSample = HKUnit.gramUnit()
            sampleType = sampleTypeProtein
            self.healthManager?.readAllNutritionByDate(sampleType!, date: self.dateForSamples, completion: ({ (nutrition, nutritionError) -> Void in
                if( nutritionError != nil )
                {
                    print("Error reading protein: \(nutritionError.localizedDescription)")
                    return
                }
                self.nutritionSamples = nutrition
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            }))
        }
        else
        {
            let sampleTypeFats = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)
            unitForSample = HKUnit.gramUnit()
            sampleType = sampleTypeFats
            self.healthManager?.readAllNutritionByDate(sampleType!, date: self.dateForSamples, completion: ({ (nutrition, nutritionError) -> Void in
                if( nutritionError != nil )
                {
                    print("Error reading protein: \(nutritionError.localizedDescription)")
                    return
                }
                self.nutritionSamples = nutrition
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            }))
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nutritionSamples.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("NutritionSampleCell", forIndexPath: indexPath)
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .MediumStyle
        dateFormatter.dateFormat = "HH:mm"
        let nutritionSampleAtIndexPath = nutritionSamples[indexPath.row] as! HKQuantitySample
        let nutritionalValue = nutritionSampleAtIndexPath.quantity.doubleValueForUnit(unitForSample)
        let date = nutritionSampleAtIndexPath.endDate
        
        cell.textLabel!.text = dateFormatter.stringFromDate(date)
        cell.detailTextLabel!.text = String(nutritionalValue) + " " + unitForSample.unitString
        //---------
        return cell
    }
    
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {}
    
    //---------------------
    // Delte nutrition samples by Swiping left
    //---------------------
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete") { (action:UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
                
            let nutritionSample = self.nutritionSamples[indexPath.row] as! HKQuantitySample
            let sampleDate = nutritionSample.startDate
            self.healthManager?.deleteNutritionSample(self.sampleType!, date: sampleDate, index: indexPath.row, completion: { (success, error ) -> Void in
                if( success )
                {
                    print("Nutrition sample deleted")
                }
                else if( error != nil )
                {
                    print("Error deleting nutrition sample" + "\(error)")
                    if (error.code == 3)
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                        let alertView = UIAlertController(title: "Error", message: "You can only delete samples entered via Fitmate. If you wish to delete this sample, please do it through the Health App", preferredStyle: .Alert)
                        alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
                        self.presentViewController(alertView, animated: true, completion: nil)
                        })
                    }
                }
                self.viewDidAppear(true)
            })
        }
        deleteAction.backgroundColor = UIColor.redColor()
        return [deleteAction]
    }
}
