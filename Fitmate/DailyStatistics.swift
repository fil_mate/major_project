//
//  DailyStatistics.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 03/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class DailyStatistics: NSObject {

    var date = NSDate()
    var totalCalories = 0.0
    var totalCarbohydrates = 0.0
    var totalProtein = 0.0
    var totalFats = 0.0
    var userHeight = 0.0
    var userWeight = 0.0
    var userSex = ""
    var userAge = 0
    var distanceFromWalking = 0.0
    var workouts = [HKWorkout]()
}
