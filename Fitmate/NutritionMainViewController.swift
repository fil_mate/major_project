//
//  NutritionMainViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 27/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class NutritionMainViewController: UITableViewController {

    @IBOutlet var nutritionMainTableView: UITableView!
    var healthManager: HealthKitManager?
    var firstDateForSamples = NSDate()
    var datesForSamples = [NSDate]()
    var dailyNutritionArray = [DailyNutrition]()
    var firstLaunchDate = NSDate()
    var dateToday = NSDate()
    let universal = FitmateUniversal()
    let calendar = NSCalendar.currentCalendar()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        healthManager = HealthKitManager()
        firstLaunchDate = (NSUserDefaults.standardUserDefaults().objectForKey("firstLaunchDate") as! NSDate)
        self.datesForSamples = universal.getDatesInBetween(firstLaunchDate, endDate: dateToday)
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        dailyNutritionArray.removeAll()
        //------------------------------
        for date in datesForSamples
        {
            let dailyNutrition = DailyNutrition()
            //---------------------------------
            // 1. Reading Calories
            //---------------------------------
            var sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryEnergyConsumed)!
            var unitForSample = HKUnit.kilocalorieUnit()
            healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date: date, completion: ({ (calories, caloriesError) -> Void in
            if(caloriesError != nil)
            {
                print("Error reading calories: \(caloriesError!.localizedDescription)")
                return
            }
            //---------------------------------
            // 2. Reading Carbohydrates
            //---------------------------------
            sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryCarbohydrates)!
            unitForSample = HKUnit.gramUnit()
            self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample,date: date, completion: ({ (carbohydrates, carbohydratesError) -> Void in
            if(carbohydratesError != nil)
            {
                print("Error reading carbohydrates: \(carbohydratesError!.localizedDescription)")
                return
            }
            //---------------------------------
            // 3. Reading Protein
            //---------------------------------
            sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryProtein)!
            self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date:date, completion: ({ (protein, proteinError) -> Void in
            if(proteinError != nil)
            {
                print("Error reading protein: \(proteinError!.localizedDescription)")
                return
            }
            //---------------------------------
            // 4. Reading Fats
            //---------------------------------
            sampleType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDietaryFatTotal)!
            self.healthManager?.getSumOfSamplesForGivenDate(sampleType, unit: unitForSample, date: date, completion: ({ (fats, fatsError) -> Void in
            if(fatsError != nil)
            {
                print("Error reading fats: \(fatsError!.localizedDescription)")
                return
            }
            print("Nutrition for " + String(date) + " read sucessfully")
            //---------------------------------------
            dailyNutrition.date = date
            dailyNutrition.totalCalories = calories
            dailyNutrition.totalCarbohydrates = carbohydrates
            dailyNutrition.totalProtein = protein
            dailyNutrition.totalFats = fats
            self.dailyNutritionArray.append(dailyNutrition)
            self.dailyNutritionArray.sortInPlace({ $0.date.compare($1.date) == .OrderedDescending })
            //---------------------------------------
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
            }))
            }))
            }))
            }))
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dailyNutritionArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("DailyNutritionCell", forIndexPath: indexPath)
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        print(indexPath.row)
        print(dailyNutritionArray.count)
        //Protecting aganist fatal error: Index out of range
        if(indexPath.row >= dailyNutritionArray.count)
        {
            print("inside the Check")
            return cell
        }
        let dailyNutrition = dailyNutritionArray[indexPath.row]
        var dateString = dateFormatter.stringFromDate(dailyNutrition.date)
        if (calendar.isDate(dailyNutrition.date, inSameDayAsDate: dateToday))
        {
            dateString = "Today"
        }
        var calories = dailyNutrition.totalCalories
        calories = Double(round(10*calories)/10)
        cell.textLabel!.text = dateString  + " | " + String(calories) + " kcal"
        //---------
        var detailtext = ""
        var carbohydrates = dailyNutrition.totalCarbohydrates
        carbohydrates = Double(round(10*carbohydrates)/10)
        detailtext += "Carbohydrates: " + String(carbohydrates) + " g | "
        //---------
        var protein = dailyNutrition.totalProtein
        protein = Double(round(10*protein)/10)
        detailtext += "Protein: " + String(protein) + " g | "
        //---------
        var fats = dailyNutrition.totalFats
        fats = Double(round(10*fats)/10)
        detailtext += "Fats: " + String(fats) + " g "
        //---------
        cell.detailTextLabel?.text = detailtext
        cell.detailTextLabel?.font = UIFont(name: "Helvetica-Light", size: 13)
        //---------
        return cell
    }
    
    //---------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showMacrosAndCaloriesView")
        {
            let upcoming: NutritionDetailsViewController = segue.destinationViewController as! NutritionDetailsViewController
            let indexPath = nutritionMainTableView.indexPathForSelectedRow
            let date = dailyNutritionArray[indexPath!.row].date
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateStyle = .MediumStyle
            var dateString = dateFormatter.stringFromDate(date)
            if (calendar.isDate(date, inSameDayAsDate: dateToday))
            {
                dateString = "Today"
            }
            let titleString = dateString
            upcoming.screenTitle = titleString
            upcoming.dateForSamples = date
            nutritionMainTableView.deselectRowAtIndexPath(indexPath!, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showMacrosAndCaloriesView", sender: self)
    }
    
    @IBAction func unwindToNutritionMain(segue: UIStoryboardSegue)
    {
        let addNutritionViewController = segue.sourceViewController as? AddNutritionViewController
        addNutritionViewController!.doSaveNutrition(addNutritionViewController!)
    }
    
    @IBAction func unwindToNutritionMainFromCustom(segue: UIStoryboardSegue)
    {
        let addCustomNutritionViewController = segue.sourceViewController as? AddCustomNutritionViewController
        addCustomNutritionViewController!.doSaveCustomNutrition(addCustomNutritionViewController!)
    }
    
}
