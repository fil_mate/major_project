//
//  FitnessMainViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 24/02/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class FitnessMainViewController: UITableViewController {

    var workouts = [HKWorkout]()
    var healthManager: HealthKitManager?
    var fitnessItems: FitnessActivities = FitnessActivities()
    var dateToday = NSDate()
    let dateFormatter = NSDateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fitness"
        healthManager = HealthKitManager()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        healthManager?.readAllActivities({ (results, error) -> Void in
            if( error != nil )
            {
                print("Error reading activities: \(error.localizedDescription)")
                return
            }
            else
            {
                print("Activities read successfully")
            }
            
            self.workouts = results as! [HKWorkout]
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return workouts.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("ActivityCell", forIndexPath: indexPath)
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = .ShortStyle
        let workout  = workouts[indexPath.row]
        let workoutDate = workout.startDate
        var dateString = dateFormatter.stringFromDate(workoutDate)
        let calendar = NSCalendar.currentCalendar()
        if (calendar.isDate(workoutDate, inSameDayAsDate: dateToday))
        {
            dateFormatter.dateFormat = "HH:mm"
            let timeString = dateFormatter.stringFromDate(workoutDate)
            // Activity Date
            dateString = "Today, " + timeString
        }
        // Activity Name
        let activity = fitnessItems.getActivityName(workout.workoutActivityType)
        cell.textLabel!.text = dateString + " | " + activity
        let durationFormatter = NSDateComponentsFormatter()
        var detailText = "Duration: "
        // Activity Duration
        let duration = durationFormatter.stringFromTimeInterval(workout.duration)!
        detailText += duration
        var distanceInKM = ""
        if (workout.totalDistance != nil )
        {
            distanceInKM = String(workout.totalDistance!.doubleValueForUnit(HKUnit.meterUnitWithMetricPrefix(HKMetricPrefix.Kilo)))
            detailText += " | Distance: " + distanceInKM + " km"
        }
        // Activity Calories
        var energyBurnedShown = 0
        if (workout.totalEnergyBurned != nil)
        {
            let energyBurned = workout.totalEnergyBurned!.doubleValueForUnit(HKUnit.kilocalorieUnit())
            energyBurnedShown = Int(energyBurned)
            detailText += " | Calories: " + String(energyBurnedShown) + " kcal"
        }
        // Set font size in table cell
        cell.detailTextLabel?.text = detailText;
        cell.detailTextLabel?.font = UIFont(name: "Helvetica-Light", size: 13)

        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {}
    
    //------------------------
    // Create delete activities functionality by Swipe-Left
    //------------------------
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: .Normal, title: "Delete") { (action:UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            self.healthManager?.deleteWorkoutSample(indexPath.row, completion: { (success, error ) -> Void in
                if( success )
                {
                    print("Workout sample deleted")
                }
                else if( error != nil )
                {
                    print("Error deleting workout sample" + "\(error)")
                    if (error.code == 3)
                    {
                        dispatch_async(dispatch_get_main_queue(), {
                            let alertView = UIAlertController(title: "Error", message: "You can only delete fitness activities entered via Fitmate. If you wish to delete this activity, please do it through the Health App", preferredStyle: .Alert)
                            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
                            self.presentViewController(alertView, animated: true, completion: nil)
                        })
                    }
                }
                self.viewDidAppear(true)
            })
        }
        deleteAction.backgroundColor = UIColor.redColor()
        return [deleteAction]
    }
    
    func unwindToSecondVC(segue: UIStoryboardSegue)
    {
        let addActivityViewController = segue.sourceViewController as? AddActivityViewController
        addActivityViewController!.doSaveActivity(addActivityViewController!)
    }
}

