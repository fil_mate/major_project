//
//  AppDelegate.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 24/02/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //Nav Bar customisation
        UINavigationBar.appearance().titleTextAttributes = ([NSFontAttributeName: UIFont(name: "Helvetica-Light", size: 25)!,
            NSForegroundColorAttributeName: UIColor.blackColor()])
        UINavigationBar.appearance().barTintColor = UIColor(hue: 161/360, saturation: 59/100, brightness: 90/100, alpha: 1.0) /* #5ee5ba */
        UINavigationBar.appearance().tintColor = UIColor.blackColor()

        //Font for entire app
        UILabel.appearance().font = UIFont(name: "Helvetica-Light", size: 15)

        //Cancel button on search bar
        let cancelButtonAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.blackColor()]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [String : AnyObject], forState: UIControlState.Normal)
        
        //--------------------------------------
        let launchedBefore = NSUserDefaults.standardUserDefaults().boolForKey("launchedBefore")
        if launchedBefore
        {
            print("Not first launch.")
        }
        else
        {
            print("First launch.")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
            let firstLaunchDate = NSDate()
            NSUserDefaults.standardUserDefaults().setObject(firstLaunchDate, forKey: "firstLaunchDate")
        }
        //---------
        return true
    }

    
    func application(application: UIApplication, willFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

