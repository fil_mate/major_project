//
//  AddNutritionViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 23/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class AddNutritionViewController: UIViewController {
    
    var healthManager: HealthKitManager?
    var foodName: String!
    @IBOutlet var nutritionDatePicker: UIDatePicker!
    @IBOutlet var dateField: UITextField!
    @IBOutlet var timeField: UITextField!
    @IBOutlet var gramsField: UITextField!
    let dateFormatter = NSDateFormatter()
    var nutritionMenu: NutritionMenuViewController = NutritionMenuViewController()
    var menuItems: NSMutableArray! = NSMutableArray()
    let universal = FitmateUniversal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = foodName
        nutritionDatePicker.hidden = true
        gramsField!.keyboardType = UIKeyboardType.DecimalPad
        healthManager = HealthKitManager()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var grams:Double
    {
        return Double(gramsField.text!)!
    }

    @IBAction func doNutritionDatePicker(sender: AnyObject)
    {
        if dateField.isFirstResponder()
        {
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateField.text! = dateFormatter.stringFromDate(nutritionDatePicker.date)
        }
        else if timeField.isFirstResponder()
        {
            dateFormatter.dateFormat = "HH:mm"
            timeField.text! = dateFormatter.stringFromDate(nutritionDatePicker.date)
        }
    }

    @IBAction func doSelectDate(sender: AnyObject)
    {
        dateField.inputView = UIView()// disables the default keyboard
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateField.text! = dateFormatter.stringFromDate(nutritionDatePicker.date)
        nutritionDatePicker.hidden = false
        nutritionDatePicker.datePickerMode = .Date
    }
    
    @IBAction func doSelectTime(sender: AnyObject)
    {
        timeField.inputView = UIView()// disables the default keyboard
        
        dateFormatter.dateFormat = "HH:mm"
        timeField.text! = dateFormatter.stringFromDate(nutritionDatePicker.date)
        nutritionDatePicker.hidden = false
        nutritionDatePicker.datePickerMode = .Time
    }
    
    @IBAction func doSelectGrams(sender: AnyObject)
    {
        nutritionDatePicker.hidden = true
    }

    func getFoodItemFromName(foodItemName: String) -> FoodItem
    {
        var foodItem = FoodItem()
        for i in 0 ..< menuItems.count
        {
            if (foodItemName == menuItems.objectAtIndex(i).name!)
            {
                foodItem = menuItems.objectAtIndex(i) as! FoodItem
            }
        }
        return foodItem
    }

    func calculateValuePerGrams(value: Double, grams: Double) -> Double
    {
        let calculatedValue = grams/100 * value
        return calculatedValue
    }
    
    @IBAction func doSaveNutrition(sender: AnyObject)
    {
        //Error handling
        //If any of the first three are empty
        if (dateField.hasText() == false || timeField.hasText() == false || gramsField.hasText() == false)
        {
            let alertView = UIAlertController(title: "Error", message: "Please type in the Time, Date and Grams", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        let foodItem = getFoodItemFromName(foodName)
        //-----------------------------------
        let calories = foodItem.calories
        let carbohydrates = foodItem.carbohydrates
        let protein = foodItem.protein
        let fats = foodItem.fats
        //-----------------------------------
        let caloriesPerGrams = calculateValuePerGrams(calories, grams: grams)
        let carbohydratesPerGrams = calculateValuePerGrams(carbohydrates, grams: grams)
        let proteinPerGrams = calculateValuePerGrams(protein, grams: grams)
        let fatsPerGrams = calculateValuePerGrams(fats, grams: grams)
        //-----------------------------------
        let finalCalories = Double(round(10*caloriesPerGrams)/10)
        let finalCarbohydrates = Double(round(10*carbohydratesPerGrams)/10)
        let finalProtein = Double(round(10*proteinPerGrams)/10)
        let finalFats = Double(round(10*fatsPerGrams)/10)
        //-----------------------------------
        var dayDate = NSDate()
        dayDate = universal.convertDate(dateField.text!)
        var timeDate = NSDate()
        timeDate = universal.convertTime(timeField.text!)
        let fullDate = universal.combineTimeWithDate(dayDate, time: timeDate)
        //-----------------------------------
        healthManager?.saveCalories(finalCalories, date: fullDate!, completion: { (success, error ) -> Void in
            if( success )
            {
                print("Calories saved")
            }
            else if( error != nil )
            {
                print("Error saving calories" + "\(error)")
            }
        })
        //-----------------------------------
        healthManager?.saveMacros(finalCarbohydrates, protein: finalProtein, fats: finalFats, date: fullDate!, completion: { (success, error ) -> Void in
            if( success )
            {
                print("Macros saved")
            }
            else if( error != nil )
            {
                print("Error saving macros" + "\(error)")
            }
        })
    }
}
