//
//  NutritionMenuViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 22/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class NutritionMenuViewController: UITableViewController {

    
    @IBOutlet var nutritionMenuTableView: UITableView!
    
    var nutritionMenu = ["Breakfast & Lunch", "Dinner", "Snacks"]
    var customEntry = ["Enter Nutrition For Food Item"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (section == 0)
        {
            return nutritionMenu.count
        }
        else
        {
            return customEntry.count
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if ( indexPath.section == 0)
        {
            performSegueWithIdentifier("showNutritionMenu", sender: self)
        }
        else
        {
            performSegueWithIdentifier("showCustomNutritionScreen", sender: self)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = nutritionMenuTableView.dequeueReusableCellWithIdentifier("nutritionMenuCell", forIndexPath: indexPath)
        
        if (indexPath.section == 0)
        {
            cell.textLabel!.text = nutritionMenu[indexPath.row]
        }
        else
        {
            cell.textLabel!.text = customEntry[0]
        }
        
        //----------
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 45
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerCell = tableView.dequeueReusableCellWithIdentifier("headerCell")
        
        headerCell!.backgroundColor = UIColor(hue: 161/360, saturation: 59/100, brightness: 90/100, alpha: 0.2)
        
        if (section == 0)
        {
            headerCell!.textLabel!.text = "Menu"

        }
        else
        {
            headerCell!.textLabel!.text  = "Custom"

        }
        return headerCell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showNutritionMenu")
        {
            let upcoming: NutritionMenuItemsViewController = segue.destinationViewController as! NutritionMenuItemsViewController
            let indexPath = nutritionMenuTableView.indexPathForSelectedRow!
            let titleString = nutritionMenu[indexPath.row]
            upcoming.screenTitle = titleString
            nutritionMenuTableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        
        if (segue.identifier == "showCustomNutritionScreen")
        {
            let upcoming: AddCustomNutritionViewController = segue.destinationViewController as! AddCustomNutritionViewController
            let indexPath = nutritionMenuTableView.indexPathForSelectedRow!
            let titleString = customEntry[indexPath.row]
            upcoming.screenTitle = titleString
            nutritionMenuTableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }

}
