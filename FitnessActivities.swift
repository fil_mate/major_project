//
//  FitnessActivities.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 26/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit
import HealthKit

class FitnessActivities: NSObject {

    var allFitnessActivities: NSMutableArray! = NSMutableArray()
    
    func populateFitnessActivities() -> NSMutableArray
    {
        allFitnessActivities.addObject("American Football")
        allFitnessActivities.addObject("Archery")
        allFitnessActivities.addObject("Australian Football")
        allFitnessActivities.addObject("Badminton")
        allFitnessActivities.addObject("Baseball")
        allFitnessActivities.addObject("Basketball")
        allFitnessActivities.addObject("Bowling")
        allFitnessActivities.addObject("Boxing")
        allFitnessActivities.addObject("Climbing")
        allFitnessActivities.addObject("Cricket")
        allFitnessActivities.addObject("Cross Training")
        allFitnessActivities.addObject("Curling")
        allFitnessActivities.addObject("Cycling")
        allFitnessActivities.addObject("Dance")
        allFitnessActivities.addObject("Dance Inspired Training")
        allFitnessActivities.addObject("Elliptical")
        allFitnessActivities.addObject("Equestrian Sports")
        allFitnessActivities.addObject("Fencing")
        allFitnessActivities.addObject("Fishing")
        allFitnessActivities.addObject("Football")
        allFitnessActivities.addObject("Functional Strength Training")
        allFitnessActivities.addObject("Golf")
        allFitnessActivities.addObject("Gymnastics")
        allFitnessActivities.addObject("Handball")
        allFitnessActivities.addObject("Hiking")
        allFitnessActivities.addObject("Hockey")
        allFitnessActivities.addObject("Hunting")
        allFitnessActivities.addObject("Lacrosse")
        allFitnessActivities.addObject("Martial Arts")
        allFitnessActivities.addObject("Mind And Body")
        allFitnessActivities.addObject("Mixed Metabolic Cardio Training")
        allFitnessActivities.addObject("Other")
        allFitnessActivities.addObject("Paddle Sports")
        allFitnessActivities.addObject("Play")
        allFitnessActivities.addObject("Preparation And Recovery")
        allFitnessActivities.addObject("Racquetball")
        allFitnessActivities.addObject("Rowing")
        allFitnessActivities.addObject("Rugby")
        allFitnessActivities.addObject("Running")
        allFitnessActivities.addObject("Sailing")
        allFitnessActivities.addObject("Skating Sports")
        allFitnessActivities.addObject("Snow Sports")
        allFitnessActivities.addObject("Softball")
        allFitnessActivities.addObject("Squash")
        allFitnessActivities.addObject("Stair Climbing")
        allFitnessActivities.addObject("Surfing Sports")
        allFitnessActivities.addObject("Swimming")
        allFitnessActivities.addObject("Table Tennis")
        allFitnessActivities.addObject("Tennis")
        allFitnessActivities.addObject("Track And Field")
        allFitnessActivities.addObject("Traditional Strenth Training")
        allFitnessActivities.addObject("Volleyball")
        allFitnessActivities.addObject("Walking")
        allFitnessActivities.addObject("Water Polo")
        allFitnessActivities.addObject("Water Sports")
        allFitnessActivities.addObject("Wresting")
        allFitnessActivities.addObject("Yoga")
        
        return allFitnessActivities
    }
    
    
    //------------------------------------
    // Convert the Workout Type to a String to show in the table
    // This fuctionality isn't provided by the API
    //------------------------------------
    func getActivityName (activityType: HKWorkoutActivityType) -> String
    {
        if (activityType == HKWorkoutActivityType.AmericanFootball){
            return "American Football"}
        if (activityType == HKWorkoutActivityType.Archery){
            return "Archery"}
        if (activityType == HKWorkoutActivityType.AustralianFootball){
            return "Australian Football"}
        if (activityType == HKWorkoutActivityType.Badminton){
            return "Badminton"}
        if (activityType == HKWorkoutActivityType.Baseball){
            return "Baseball"}
        if (activityType == HKWorkoutActivityType.Basketball){
            return "Basketball"}
        if (activityType == HKWorkoutActivityType.Bowling){
            return "Bowling"}
        if (activityType == HKWorkoutActivityType.Boxing){
            return "Boxing"}
        if (activityType == HKWorkoutActivityType.Climbing){
            return "Climbing"}
        if (activityType == HKWorkoutActivityType.Cricket){
            return "Cricket"}
        if (activityType == HKWorkoutActivityType.CrossTraining){
            return "Cross Training"}
        if (activityType == HKWorkoutActivityType.Curling){
            return "Curling"}
        if (activityType == HKWorkoutActivityType.Cycling){
            return "Cycling"}
        if (activityType == HKWorkoutActivityType.Dance){
            return "Dance"}
        if (activityType == HKWorkoutActivityType.DanceInspiredTraining){
            return "Dance Inspired Training"}
        if (activityType == HKWorkoutActivityType.Elliptical){
            return "Elliptical"}
        if (activityType == HKWorkoutActivityType.EquestrianSports){
            return "Equestrian Sports"}
        if (activityType == HKWorkoutActivityType.Fencing){
            return "Fencing"}
        if (activityType == HKWorkoutActivityType.Fishing){
            return "Fishing"}
        if (activityType == HKWorkoutActivityType.Soccer){
            return "Football"}
        if (activityType == HKWorkoutActivityType.FunctionalStrengthTraining){
            return "Functional Strength Training"}
        if (activityType == HKWorkoutActivityType.Golf){
            return "Golf"}
        if (activityType == HKWorkoutActivityType.Gymnastics){
            return "Gymnastics"}
        if (activityType == HKWorkoutActivityType.Handball){
            return "Handball"}
        if (activityType == HKWorkoutActivityType.Hiking){
            return "Hiking"}
        if (activityType == HKWorkoutActivityType.Hockey){
            return "Hockey"}
        if (activityType == HKWorkoutActivityType.Hunting){
            return "Hunting"}
        if (activityType == HKWorkoutActivityType.Lacrosse){
            return "Lacrosse"}
        if (activityType == HKWorkoutActivityType.MartialArts){
            return "Martial Arts"}
        if (activityType == HKWorkoutActivityType.MindAndBody){
            return "Mind And Body"}
        if (activityType == HKWorkoutActivityType.MixedMetabolicCardioTraining){
            return "Mixed Metabolic Cardio Training"}
        if (activityType == HKWorkoutActivityType.Other){
            return "Other"}
        if (activityType == HKWorkoutActivityType.PaddleSports){
            return "Paddle Sports"}
        if (activityType == HKWorkoutActivityType.Play){
            return "Play"}
        if (activityType == HKWorkoutActivityType.PreparationAndRecovery){
            return "Preparation And Recovery"}
        if (activityType == HKWorkoutActivityType.Racquetball){
            return "Racquetball"}
        if (activityType == HKWorkoutActivityType.Rowing){
            return "Rowing"}
        if (activityType == HKWorkoutActivityType.Rugby){
            return "Rugby"}
        if (activityType == HKWorkoutActivityType.Running){
            return "Running"}
        if (activityType == HKWorkoutActivityType.Sailing){
            return "Sailing"}
        if (activityType == HKWorkoutActivityType.SkatingSports){
            return "Skating Sports"}
        if (activityType == HKWorkoutActivityType.SnowSports){
            return "Snow Sports"}
        if (activityType == HKWorkoutActivityType.Softball){
            return "Softball"}
        if (activityType == HKWorkoutActivityType.Squash){
            return "Squash"}
        if (activityType == HKWorkoutActivityType.StairClimbing){
            return "Stair Climbing"}
        if (activityType == HKWorkoutActivityType.SurfingSports){
            return "Surfing Sports"}
        if (activityType == HKWorkoutActivityType.Swimming){
            return "Swimming"}
        if (activityType == HKWorkoutActivityType.TableTennis){
            return "Table Tennis"}
        if (activityType == HKWorkoutActivityType.Tennis){
            return "Tennis"}
        if (activityType == HKWorkoutActivityType.TrackAndField){
            return "Track And Field"}
        if (activityType == HKWorkoutActivityType.TraditionalStrengthTraining){
            return "Traditional Strength Training"}
        if (activityType == HKWorkoutActivityType.Volleyball){
            return "Volleyball"}
        if (activityType == HKWorkoutActivityType.Walking){
            return "Walking"}
        if (activityType == HKWorkoutActivityType.WaterPolo){
            return "Water Polo"}
        if (activityType == HKWorkoutActivityType.WaterSports){
            return "Water Sports"}
        if (activityType == HKWorkoutActivityType.Wrestling){
            return "Wrestling"}
        if (activityType == HKWorkoutActivityType.Yoga){
            return "Yoga"}
        //-----------------------------
        return "Error in getActivityName"
    }
    
    //------------------------------------
    // Convert String of activity name to HealthKit workout type
    //------------------------------------
    func getActivityType (name: String) -> HKWorkoutActivityType
    {
        if (name == "American Football"){
            return HKWorkoutActivityType.AmericanFootball
        }
        if (name == "Archery"){
            return HKWorkoutActivityType.Archery
        }
        if (name == "Australian Football"){
            return HKWorkoutActivityType.AustralianFootball
        }
        if (name == "Badminton"){
            return HKWorkoutActivityType.Badminton
        }
        if (name == "Baseball"){
            return HKWorkoutActivityType.Baseball
        }
        if (name == "Basketball"){
            return HKWorkoutActivityType.Basketball
        }
        if (name == "Bowling"){
            return HKWorkoutActivityType.Bowling
        }
        if (name == "Boxing"){
            return HKWorkoutActivityType.Boxing
        }
        if (name == "Climbing"){
            return HKWorkoutActivityType.Climbing
        }
        if (name == "Cricket"){
            return HKWorkoutActivityType.Cricket
        }
        if (name == "Cross Training"){
            return HKWorkoutActivityType.CrossTraining
        }
        if (name == "Curling"){
            return HKWorkoutActivityType.Curling
        }
        if (name == "Cycling"){
            return HKWorkoutActivityType.Cycling
        }
        if (name == "Dance"){
            return HKWorkoutActivityType.Dance
        }
        if (name == "Dance Inspired Training"){
            return HKWorkoutActivityType.DanceInspiredTraining
        }
        if (name == "Elliptical"){
            return HKWorkoutActivityType.Elliptical
        }
        if (name == "Equestrian Sports"){
            return HKWorkoutActivityType.EquestrianSports
        }
        if (name == "Fencing"){
            return HKWorkoutActivityType.Fencing
        }
        if (name == "Fishing"){
            return HKWorkoutActivityType.Fishing
        }
        if (name == "Football"){
            return HKWorkoutActivityType.Soccer
        }
        if (name == "Functional Strength Training"){
            return HKWorkoutActivityType.FunctionalStrengthTraining
        }
        if (name == "Golf"){
            return HKWorkoutActivityType.Golf
        }
        if (name == "Gymnastics"){
            return HKWorkoutActivityType.Gymnastics
        }
        if (name == "Handball"){
            return HKWorkoutActivityType.Handball
        }
        if (name == "Hiking"){
            return HKWorkoutActivityType.Hiking
        }
        if (name == "Hockey"){
            return HKWorkoutActivityType.Hockey
        }
        if (name == "Hunting"){
            return HKWorkoutActivityType.Hunting
        }
        if (name == "Lacrosse"){
            return HKWorkoutActivityType.Lacrosse
        }
        if (name == "Martial Arts"){
            return HKWorkoutActivityType.MartialArts
        }
        if (name == "Mind And Body"){
            return HKWorkoutActivityType.MindAndBody
        }
        if (name == "Mixed Metabolic Cardio Training"){
            return HKWorkoutActivityType.MixedMetabolicCardioTraining
        }
        if (name == "Other"){
            return HKWorkoutActivityType.Other
        }
        if (name == "Paddle Sports"){
            return HKWorkoutActivityType.PaddleSports
        }
        if (name == "Play"){
            return HKWorkoutActivityType.Play
        }
        if (name == "Preparation And Recovery"){
            return HKWorkoutActivityType.PreparationAndRecovery
        }
        if (name == "Racquetball"){
            return HKWorkoutActivityType.Racquetball
        }
        if (name == "Rowing"){
            return HKWorkoutActivityType.Rowing
        }
        if (name == "Rugby"){
            return HKWorkoutActivityType.Rugby
        }
        if (name == "Running"){
            return HKWorkoutActivityType.Running
        }
        if (name == "Sailing"){
            return HKWorkoutActivityType.Sailing
        }
        if (name == "Skating Sports"){
            return HKWorkoutActivityType.SkatingSports
        }
        if (name == "Snow Sports"){
            return HKWorkoutActivityType.SnowSports
        }
        if (name == "Softball"){
            return HKWorkoutActivityType.Softball
        }
        if (name == "Squash"){
            return HKWorkoutActivityType.Squash
        }
        if (name == "Stair Climbing"){
            return HKWorkoutActivityType.StairClimbing
        }
        if (name == "Surfing Sports"){
            return HKWorkoutActivityType.SurfingSports
        }
        if (name == "Swimming"){
            return HKWorkoutActivityType.Swimming
        }
        if (name == "Table Tennis"){
            return HKWorkoutActivityType.TableTennis
        }
        if (name == "Tennis"){
            return HKWorkoutActivityType.Tennis
        }
        if (name == "Track And Field"){
            return HKWorkoutActivityType.TrackAndField
        }
        if (name == "Traditional Strenth Training"){
            return HKWorkoutActivityType.TraditionalStrengthTraining
        }
        if (name == "Volleyball"){
            return HKWorkoutActivityType.Volleyball
        }
        if (name == "Walking"){
            return HKWorkoutActivityType.Walking
        }
        if (name == "Water Polo"){
            return HKWorkoutActivityType.WaterPolo
        }
        if (name == "Water Sports"){
            return HKWorkoutActivityType.WaterSports
        }
        if (name == "Wresting"){
            return HKWorkoutActivityType.Wrestling
        }
        if (name == "Yoga"){
            return HKWorkoutActivityType.Yoga
        }
        return HKWorkoutActivityType.Other
    }


}
