//
//  NutritionDetailsViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 04/04/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class NutritionDetailsViewController: UITableViewController {

    @IBOutlet var nutritionDetailsTableView: UITableView!
    var screenTitle = ""
    var nutritionArray = ["Calories", "Carbohydrates", "Protein", "Fats"]
    var dateForSamples = NSDate()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = screenTitle
        print(dateForSamples)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nutritionArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("nutritionEditCell", forIndexPath: indexPath)
       cell.textLabel?.text = nutritionArray[indexPath.row]
        //---------
        return cell
    }
    
    //---------------------------
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if (segue.identifier == "showNutritionSamplesForDayView")
        {
            let upcoming: NutritionSamplesViewController = segue.destinationViewController as! NutritionSamplesViewController
            let indexPath = nutritionDetailsTableView.indexPathForSelectedRow
            let dateToPass = dateForSamples
            let titleString = nutritionArray[indexPath!.row]
            upcoming.screenTitle = titleString
            upcoming.dateForSamples = dateToPass
            nutritionDetailsTableView.deselectRowAtIndexPath(indexPath!, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        performSegueWithIdentifier("showNutritionSamplesForDayView", sender: self)
    }

}
