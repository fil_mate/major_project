//
//  AddCustomNutritionViewController.swift
//  Fitmate
//
//  Created by Filip Kunkiewicz on 22/03/2016.
//  Copyright © 2016 Filip Kunkiewicz. All rights reserved.
//

import UIKit

class AddCustomNutritionViewController: UIViewController {

    var screenTitle: String!
    var healthManager: HealthKitManager?
    let dateFormatter = NSDateFormatter()

    @IBOutlet var dateField: UITextField!
    @IBOutlet var timeField: UITextField!
    @IBOutlet var caloriesField: UITextField!
    @IBOutlet var carbohydratesField: UITextField?
    @IBOutlet var proteinField: UITextField?
    @IBOutlet var fatsField: UITextField?
    @IBOutlet var customNutritionDatePicker: UIDatePicker!
    let universal = FitmateUniversal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        healthManager = HealthKitManager()
        customNutritionDatePicker.hidden = true
        
        caloriesField!.keyboardType = UIKeyboardType.DecimalPad
        carbohydratesField!.keyboardType = UIKeyboardType.DecimalPad
        proteinField!.keyboardType = UIKeyboardType.DecimalPad
        fatsField!.keyboardType = UIKeyboardType.DecimalPad

        self.title = "Custom Entry"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var calories:Double
    {
        return Double(caloriesField.text!)!
    }
    
    var carbohydrates:Double?
    {
        return Double(carbohydratesField!.text!)
    }
    
    var protein:Double?
    {
        return Double(proteinField!.text!)
    }
    
    var fats:Double?
    {
        return Double(fatsField!.text!)
    }
    
    
    @IBAction func doCustomNutritionDatePicker(sender: AnyObject)
    {
        if dateField.isFirstResponder()
        {
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateField.text! = dateFormatter.stringFromDate(customNutritionDatePicker.date)
        }
        else if timeField.isFirstResponder()
        {
            dateFormatter.dateFormat = "HH:mm"
            timeField.text! = dateFormatter.stringFromDate(customNutritionDatePicker.date)
        }
    }
    
    @IBAction func doSelectDate(sender: AnyObject)
    {
        dateField.inputView = UIView()// disables the default keyboard
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateField.text! = dateFormatter.stringFromDate(customNutritionDatePicker.date)
        customNutritionDatePicker.hidden = false
        customNutritionDatePicker.datePickerMode = .Date
    }
    
    @IBAction func doSelectTime(sender: AnyObject)
    {
        timeField.inputView = UIView()// disables the default keyboard
        
        dateFormatter.dateFormat = "HH:mm"
        timeField.text! = dateFormatter.stringFromDate(customNutritionDatePicker.date)
        customNutritionDatePicker.hidden = false
        customNutritionDatePicker.datePickerMode = .Time
    }
    
    @IBAction func doSelectCalories(sender: AnyObject)
    {
        customNutritionDatePicker.hidden = true
    }
    
    @IBAction func doSelectCarbohydrates(sender: AnyObject)
    {
        customNutritionDatePicker.hidden = true
    }
    
    @IBAction func doSelectProtein(sender: AnyObject)
    {
        customNutritionDatePicker.hidden = true
    }
    
    @IBAction func doSelectFats(sender: AnyObject)
    {
        customNutritionDatePicker.hidden = true
    }

    @IBAction func doSaveCustomNutrition(sender: AnyObject)
    {
        //Error handling
        //If any of the first three are empty
        if (dateField.hasText() == false || timeField.hasText() == false || caloriesField.hasText() == false)
        {
            let alertView = UIAlertController(title: "Error", message: "Please type in the Time, Date and Calories", preferredStyle: .Alert)
            alertView.addAction(UIAlertAction(title: "Dismiss", style: .Default, handler: nil))
            presentViewController(alertView, animated: true, completion: nil)
            return
        }
        //-----------------------------------
        var dayDate = NSDate()
        dayDate = universal.convertDate(dateField.text!)
        var timeDate = NSDate()
        timeDate = universal.convertTime(timeField.text!)
        let fullDate = universal.combineTimeWithDate(dayDate, time: timeDate)
        //-----------------------------------
        healthManager?.saveCalories(calories, date: fullDate!, completion: { (success, error ) -> Void in
            if( success )
            {
                print("Custom calories saved")
            }
            else if( error != nil )
            {
                print("Error saving calories" + "\(error)")
            }
        })
        //-----------------------------------
        if (carbohydratesField!.hasText() == true || proteinField!.hasText() == true || fatsField!.hasText() == true)
        {
            healthManager?.saveMacros(carbohydrates, protein: protein, fats: fats, date: fullDate!, completion: { (success, error ) -> Void in
                if( success )
                {
                    print("Custom macros saved")
                }
                else if( error != nil )
                {
                    print("Error saving macros" + "\(error)")
                }
            })
        }
    }
    
}
